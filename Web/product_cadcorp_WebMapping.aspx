﻿<%@ Page Title="CadcorpSIS Web Mapping" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="product_cadcorp_WebMapping.aspx.vb" Inherits="product_cadcorp_WebMapping" %>

<%@ Register src="wuc_CadcorpMetaTag.ascx" tagname="wuc_HeadMetaTag" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<uc1:wuc_HeadMetaTag ID="wuc_HeadMetaTag1" runat="server" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

    <style type="text/css">
            .page-top
            {
                margin-bottom:0px;
                }
            .custom-product
            {               
                margin-bottom:35px;
                }
                
            .page-top.custom-product
            {
                 background-image: url(img/cadcorp/bg_blue_1920x700.jpg);
                }            
    </style>

     <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">									
						<li class="active">CadcorpSIS</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Web Mapping</h1>
				</div>
			</div>
		</div>
	</section>

    <section class="page-top custom-product">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<h1>Web-base Solution <strong> จาก CadcorpSIS</strong>.</h1>
					<p class="lead">โซลูชั่นระดับ Enterprise ช่วยให้คุณใช้ประโยชน์จากข้อมูลแผนที่ GIS ที่องค์กรมีอยู่ได้ประโยชน์สูงสุด
                    โดยการนำเสนอผ่านระบบเครือข่ายทั้ง Internet และ Intranet โดยการพัฒนาซอฟท์แวร์เพื่อตอบความต้องการทางธุรกิจของคุณอย่างมีประสิทธิภาพ
                    </p>					
                </div>
				<div class="col-sm-5">
					<img class="pull-right responsive" alt="GIS Web-base Solution" src="img/cadcorp/WML_carousel_overlay.png">
				</div>
			</div>
		</div>
	</section>

    <div class="container">
        <div class="row">
		    <div class="col-sm-7">
			    <h2>แชร์ข้อมูลด้วย <strong>Web Mapping</strong></h2>
			    <p class="lead">
				    Web Mapping เป็น Solution สำหรับนำข้อมูลแผนที่ GIS ที่คุณมี ไปใช้ประโยชน์ในแอพพลิเคชั่นทางธุรกิจขององค์กร 
                    ที่สามารถจำกัดสิทธิ์การเข้าถึงได้ โดยการเขียนแอพพลิเคชั่นด้วย Cadcorp Developer Tools ซึ่งการทำงานกับข้อมูลที่เผยแพร่
                    บนระบบเครือข่ายมี 3 รูปแบบคือ
               </p>
		    </div>

		    <div class="col-sm-4 col-sm-offset-1 push-top">
			    <img class="img-responsive" src="img/cadcorp/cadcorp_web_200x200.png">
		    </div>
	    </div>

        <hr class="tall" />

        <div class="row custom-product"> 
		    <div class="col-sm-6">
			    <h2><strong>Web Map Layer</strong> </h2>			    
				    <div class="row">
			            <div class="col-md-12">				    				            
						 เป็นการแชร์แผนที่ Digital ในรูปแบบ Vector Format เพื่อให้แอพพลิเคชั่นอื่นๆ สามารถนำข้อมูลดังกล่าวไปใช้ในแอพพลิเคชั่นของตอนได้
                         โดยผ่าน Web Service ไม่ว่าจะเป็นการนำแผนที่ WML ไปซ้อนทับใน Google Map หรือนำไปใช้ร่วมกับแอพพลิเคชั่นอื่นๆ ได้อย่างกว้างขวาง
                         เป็นการพัฒนาระบบ GIS ตามเป้าหมายมาตรฐาน OGC(Open GIS Consortium)   
                         
                             <div class="col-sm-6 push-top">
                                 <a href="http://www.cadcorp.com/products/web-mapping/" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-envelope"></i>เพิ่มเติม</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
                            </div>               	
			            </div>

		            </div>
		    </div>
            <div class="col-sm-6 push-top">
                 <img class="img-responsive" src="img/cadcorp/wms-diagram.png" alt="Web Map Service Concept" >
            </div>
	    </div>

        <div class="row custom-product"> 
		    <div class="col-sm-6">
			    <h2><strong>Web Map Viewer & Editor</strong> </h2>			    
				    <div class="row">
			            <div class="col-md-12">				    				            
						 เป็นการพัฒนาแอพพลิเคชั่นบน Browser-Base (แอพพลิเคชั่นที่ทำงานบน Web Browser เช่น Internet Explorer, Firefox, Chrome หรือ Safaru) 
                         ด้วย Developer Tools หรือ SDK โดยฝั่งผู้ใช้ไม่ต้องติดตั้งโปรแกรมเพิ่มเติมก็สามารถทำงานได้ ช่วยให้การดูแลระบบทำได้ง่ายขึ้น
                         <br>   <br>                       
                         โดยมีเป้าหมายเพื่อประยุกต์ใช้แผนที่ GIS ที่มีอยู่ร่วมกัน แล้วแต่ขอบเขตความต้องการที่ได้ออกแบบไว้ เช่นการแสดงแผนที่ภาพรวมเชิงสถิติ การวิเคราะห์แผนที่โดยการซ้อนทับของชั้นข้อมูล
                         หรือการวิเคราะห์ขั้นสูงโดยใช้ Topology ทางภูมิศาตร์ การวิเคราะห์ความหนาแน่นข้อมูล แผนที่แสดงลักษณะทางภูมิศาตร์และกายภาพ และอื่นๆ อีกมาก
                         <br> <br> 
                         ซึ่งที่ผ่านมาใน Version ก่อนหน้านี้ Cadcorp อนุญาติให้เผยแพร่ข้อมูลได้อย่างเดียวแม่สามารถแก้ไขข้อมูลแผนที่ผ่านหน้าเว็บได้ 
                         แต่เวอร์ชั่นปัจจุบันผู้ใช้สามารถแก้ไขข้อมูลผ่านหน้าเว็บได้ด้วย ทั้งนี้ขึ้นอยู่กับสิทธิ์ที่ถูกกำหนดเอาไว้เช่นกัน

                        <div class="col-sm-6 push-top">
                                <a href="http://www.cadcorp.com/products/web-mapping/" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-envelope"></i>เพิ่มเติม</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
                        </div>     

			            </div>
		            </div>
		    </div>
            <div class="col-sm-6 push-top">
                 <img class="img-responsive" src="img/cadcorp/WebMapEditor.png" alt="Web Map Viewer & Editor" />
            </div>
	    </div>

        <div class="row custom-product"> 
		    <div class="col-sm-6">
			    <h2><strong>Web Map Express</strong> </h2>			    
				    <div class="row">
			            <div class="col-md-12">				    				            
						 เป็นการพัฒนาแอพพลิเคชั่นบน Browser-Base  ด้วย Developer Tools หรือ SDK เช่นเดียวกันกับ Web Map Viewer & Editor 
                         แต่ไม่สามารถแก้ไขข้อมูลแผนที่ได้เหมือน Map Viewer & Editor 
                         ซึ่งเหมาะกับการพัฒนาแอพพลิเคชั่นทั่วไปใช้แสดงผลข้อมูลแผนที่ ซึ่งค่าใช้จ่ายสำหรับ License จะด่ำกว่า Web Map Viewer & Editor
                         ซึ่งจะช่วยลดค่าใช้จ่ายในโครงการได้

                         <div class="col-sm-6 push-top">
                                <a href="http://www.cadcorp.com/products/web-mapping/" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-envelope"></i>เพิ่มเติม</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
                        </div>  

			            </div>
		            </div>
		    </div>
            <div class="col-sm-6 push-top">
                 <img class="img-responsive" src="img/cadcorp/webmapexpress.png" alt="" />
            </div>
	    </div>
    </div>

    <section class="call-to-action featured footer">
		<div class="container">
			<div class="row">
				<div class="center">
					<h3> 
                    หากคุณสนใจการพัฒนาโซลูชั่นสำหรับงาน Web Mapping คุณสามารถ
                    <a href="contact.aspx" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">สอบถามข้อมูลเพิ่มเติม</a> </strong>
                    <br>เรายินดีให้คำปรึกษาโดยไม่มีค่าใช้จ่ายใดๆ
                    </h3>
				</div>
			</div>
		</div>
	</section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">
</asp:Content>

