﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register src="wuc_HeadMetaTag.ascx" tagname="wuc_HeadMetaTag" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<uc1:wuc_HeadMetaTag ID="wuc_HeadMetaTag1" runat="server" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

<div class="slider-container">
					<div class="slider" id="revolutionSlider" data-plugin-revolution-slider data-plugin-options='{"startheight": 500}'>
						<ul>
							<li data-transition="fade" data-slotamount="13" data-masterspeed="300" >
				
								<img src="img/slides/slide-bg.jpg" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
				
								<div class="tp-caption sft stb visible-lg"
									 data-x="177"
									 data-y="170"
									 data-speed="500"
									 data-start="1000"
									 data-easing="easeOutExpo"><img src="img/slides/slide-title-border.png" alt=""></div>
				
								<div class="tp-caption top-label lfl stl"
									 data-x="227"
									 data-y="170"
									 data-speed="500"
									 data-start="500"
									 data-easing="easeOutExpo">หากคุณต้องการพัฒนา</div>
				
								<div class="tp-caption sft stb visible-lg"
									 data-x="490"
									 data-y="170"
									 data-speed="500"
									 data-start="1000"
									 data-easing="easeOutExpo"><img src="img/slides/slide-title-border.png" alt=""></div>
				
								<div class="tp-caption main-label sft stb"
									 data-x="135"
									 data-y="210"
									 data-speed="500"
									 data-start="1500"
									 data-easing="easeOutExpo">ขีดความสามารถทางธุรกิจ</div>
				
								<div class="tp-caption bottom-label sft stb"
									 data-x="185"
									 data-y="280"
									 data-speed="500"
									 data-start="3000"
									 data-easing="easeOutExpo">ซอฟท์แวร์ที่ดีและเทคโนโลยีที่เหมาะสมช่วยคุณได้</div>
				
								<div class="tp-caption randomrotate"
									 data-x="905"
									 data-y="248"
									 data-speed="500"
									 data-start="4500"
									 data-easing="easeOutBack"><img src="img/slides/slide-concept-2-1.png" alt=""></div>
				
								<div class="tp-caption sfb"
									 data-x="955"
									 data-y="200"
									 data-speed="400"
									 data-start="4600"
									 data-easing="easeOutBack"><img src="img/slides/slide-concept-2-2.png" alt=""></div>
				
								<div class="tp-caption sfb"
									 data-x="925"
									 data-y="170"
									 data-speed="700"
									 data-start="4700"
									 data-easing="easeOutBack"><img src="img/slides/slide-concept-2-3.png" alt=""></div>
				
								<div class="tp-caption sfb"
									 data-x="875"
									 data-y="130"
									 data-speed="1000"
									 data-start="4850"
									 data-easing="easeOutBack"><img src="img/slides/slide-concept-2-4.png" alt=""></div>
				
								<div class="tp-caption sfb"
									 data-x="605"
									 data-y="20"
									 data-speed="600"
									 data-start="5000"
									 data-easing="easeOutExpo"><img src="img/slides/slide-concept-2-5.png" alt=""></div>
				
								<div class="tp-caption blackboard-text lfb "
									 data-x="635"
									 data-y="330"
									 data-speed="500"
									 data-start="5050"
									 data-easing="easeOutExpo" style="font-size: 37px;">ลอง</div>
				
								<div class="tp-caption blackboard-text lfb "
									 data-x="660"
									 data-y="380"
									 data-speed="500"
									 data-start="5250"
									 data-easing="easeOutExpo" style="font-size: 47px;">คิดนอกกรอบ</div>
				
								<div class="tp-caption blackboard-text lfb "
									 data-x="685"
									 data-y="430"
									 data-speed="500"
									 data-start="6250"
									 data-easing="easeOutExpo" style="font-size: 32px;">เพื่อสร้างนวัตกรรมในองค์กร :)</div>
							</li>
							<li data-transition="fade" data-slotamount="5" data-masterspeed="1000" >
				
								
				
								<img src="img/slides/slide-bg.jpg" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
				
									<div class="tp-caption sft stb"
										 data-x="155"
										 data-y="100"
										 data-speed="600"
										 data-start="100"
										 data-easing="easeOutExpo"><img src="img/slides/slide-concept.png" alt=""></div>
				
									<div class="tp-caption blackboard-text sft stb"
										 data-x="285"
										 data-y="180"
										 data-speed="900"
										 data-start="1000"
										 data-easing="easeOutExpo" style="font-size: 30px;">เพิ่มศักยภาพ</div>
				
									<div class="tp-caption blackboard-text sft stb"
										 data-x="285"
										 data-y="220"
										 data-speed="900"
										 data-start="1500"
										 data-easing="easeOutExpo" style="font-size: 40px;">ลดต้นทุน</div>
				
									<div class="tp-caption main-label sft stb"
										 data-x="685"
										 data-y="190"
										 data-speed="300"
										 data-start="3000"
										 data-easing="easeOutExpo">ปรึกษาเราฟรี!</div>
				                    
                                    <div class="tp-caption bottom-label sft stb"
										 data-x="685"
										 data-y="270"
										 data-speed="500"
										 data-start="3500"
										 data-easing="easeOutExpo">ไม่จำเป็นต้องลงทุนสูง !!</div>

									<div class="tp-caption bottom-label sft stb"
										 data-x="685"
										 data-y="320"
										 data-speed="500"
										 data-start="4000"
										 data-easing="easeOutExpo">และไม่ยากอย่างที่คิด !!</div>
				
							</li>
						</ul>
					</div>
				</div>
				<div class="home-intro" id="home-intro">
					<div class="container">
				
						<div class="row">
							<div class="col-md-8">
								<p>
									ประสบการณ์และจินตนาการ สร้างสรรค์ <em>Technology</em>
									<span>ความสำเร็จคือพื้นฐาน พัฒนาการคือเป้าหมาย</span>
								</p>
							</div>
							<div class="col-md-4">
								<div class="get-started">
									<a href="Contact.aspx" class="btn btn-lg btn-primary">ติดต่อเรา!</a>
									<div class="learn-more">หรือ </div>
                                    <a href="about_vision.aspx" class="btn btn-lg btn-primary">ทำความรู้จักเรา</a>
								</div>
							</div>
						</div>
				
					</div>
				</div>
				
				<div class="container">				
					<div class="row center">
						<div class="col-md-12">
							<h1 class="short word-rotator-title">
								เป้าหมายการทำงานของเราเพื่อ
								<strong class="inverted">
									<span class="word-rotate" data-plugin-options='{"delay": 2000, "animDelay": 300}'>
										<span class="word-rotate-items">
											<span>ลดต้นทุน</span>
											<span>ควบคุมระบบการทำงาน</span>
											<span>ลดความซ้ำซ้อน</span>
                                            <span>สะท้อนภาพรวม</span>
                                            <span>วิเคราะห์แนวโน้ม</span>
                                            <span>เปิดช่องทางใหม่ๆ</span>
										</span>
									</span>
								</strong>
								ให้ธุรกิจของคุณ
							</h1>
							<%--<p class="featured lead">
								"ปัจจุบันเป็นยุคของข้อมูลข่าวสาร ผู้มีข้อมูลคือผู้ชนะ" สะท้อนถึงสภาวะธุรกิจในปัจจุบัน ย่อมเป็นการดีที่ธุรกิจต้องปรับตัวให้สอดคล้องกับสภาพการแข่งขัน
                                แต่ความสำเร็จและการพัฒนาอย่างต่อเนื่องเป็นสิ่งที่สำคัญยิ่งกว่า คำว่า Enterprise ไม่ได้มาโดยง่าย ต้องอาศัยการพัฒนาอย่างต่อเนื่องเพื่อรักษาตำแหน่งความได้เปรียบทางธุรกิจเอาไว้
							</p>
                            <p class="featured lead">
								แล้ว Enterprise System คืออะไร นี่คือคำถามสำคัญ น้อยคนนักที่เข้าใจความหมายที่ถ่องแท้ Enterprise ไม่ได้หมายถึงความยิ่งใหญ่อลังการมูลค่าสูง แต่หากหมายถึง
                                "ความครอบคลุมทั่วถึงในระบบ" ตั้งแต่การควบคุมขั้นปฏิบัติการให้เป็นระบบที่ดีที่สุด การมองภาพรวมเชิงนโยบายและการบริหาร ไปจนถึงความสามารถที่ได้จากการสั่งสมประสบการณ์
                                เพื่อเสนอแนวทางในการเสนอแนวทางแก้ปัญหา โดยอาศัยวิธีการทำงานอย่างเป็นระบบ
                            </p>
                            <p class="featured lead">
								บริษัท ไทย อิเมจิเนชั่นเทคโนโลยี จำกัด จัดตั้งขึ้นโดยการรวมตัวของทีมงานผู้มีประสบการณ์หลายสาขาอาชีพ เพื่อแก้ปัญหาการดำเนินธุรกิจให้แก่ลูกค้า โดยมิได้อาศัยเพียงแค่การนำเทคโนโลยี
                                มาใช้เท่านั้น แต่ทุกแนวทางต้องผ่านการวิเคราะห์แนวทางจากที่ปรึกษาทางโลจิสติกส์ควบคู่กันไป เพื่อตอบวัตถุประสงค์ของธุรกิจได้ตรงความต้องการของลูกค้ามากที่สุด  
                            </p>
                            <p class="featured lead">
								ปัจจุบันบริษัทฯ ดำเนินธุรกิจมาเป็นปีที่ 3 ซึ่งลูกค้าของเรามีตั้งแต่ธุรกิจขนาดเล็ก ร้านค้าปลีก ร้านอาหาร บริษัท SME ไปจนถึง ยักษ์ใหญ่ในวงการพลังงานของประเทศ และครอบคลุมถึง
                                องค์กรธุรกิจในต่างประเทศ โดยเราให้ความสำคัญกับลูกค้าทุกระดับ ซึ่งเราสามารถกล่าวได้เต็มภาคภูมิว่าระบบที่เราพัฒนาเราสามารถส่งมอบได้สำเร็จ 100% 
                                และลูกค้าทุกรายเราถือว่าเป็นลูกค้าระดับ Enterprise เนื่องจากปัจจุบันทุกรายยังมอบความไว้วางใจให้เรา เป็นที่ปรึกษาและพัฒนาระบบธุรกิจอย่างต่อเนื่อง ตรงกับวัตถุประสงค์ที่เราวางไว้
                            </p>--%>
                            <p class="featured lead">
                                ปัจจุบันบริษัท ไทย อิเมจิเนชั่นเทคโนโลยี จำกัด ดำเนินธุรกิจด้านเทคโนโลยีและการพัฒนาซอฟท์แวร์เพื่อใช้ในธุรกิจมาเป็นปีที่ 3 ซึ่งลูกค้าของเราประกอบด้วยธุรกิจทุกระดับ ตั้งแต่ร้านค้าปลีก บริษัท SME หน่วยงานราชการ รัฐวิสาหกิจ ไปจนถึงยักษ์ใหญ่ในวงการอุตสาหกรรมและพลังงาน 
                                ครอบคลุมถึงองค์กรธุรกิจในต่างประเทศ โดยเราให้ความสำคัญกับลูกค้าทุกระดับ ซึ่งเราสามารถกล่าวได้เต็มภาคภูมิว่า<u>งานที่เราพัฒนาสำเร็จ 100%</u> 
                                ซึ่งเราเองก็ภูมิใจในความสำเร็จของลูกค้า และเราถือว่าทุกรายเป็นลูกค้าระดับ Enterprise เนื่องจากปัจจุบัน<u>ลูกค้าทุกราย</u>ยังมอบความไว้วางใจให้เรา เป็นที่ปรึกษาและพัฒนาระบบธุรกิจอย่างต่อเนื่อง 
                            </p>
                            <%--<p class="featured lead">
                                "ปัจจุบันเป็นยุคของข้อมูลข่าวสาร ผู้มีข้อมูลคือผู้ได้เปรียบ" สะท้อนถึงสภาวะการแข่งขันในปัจจุบัน ย่อมเป็นการดีที่ธุรกิจต้องปรับตัวให้สอดคล้องกับสภาพการแข่งขัน
                                แต่การพัฒนาอย่างต่อเนื่องเป็นสิ่งที่สำคัญยิ่งกว่า คำว่า Enterprise ไม่ได้มาโดยง่าย ต้องอาศัย<u>การพัฒนาอย่างต่อเนื่อง</u>เพื่อรักษาตำแหน่งความได้เปรียบทางธุรกิจเอาไว้                                
                            </p>
                            <p class="featured lead">
								แล้ว Enterprise System คืออะไร น้อยคนนักที่เข้าใจความหมายแท้จริง Enterprise System ไม่ได้หมายโครงการที่มีมูลค่าสูง แต่หากหมายถึง
                                "ระบบที่ครอบคลุมทั่วถึง" ตั้งแต่การควบคุมขั้นตอนปฏิบัติการให้สุด การมองภาพรวมเชิงนโยบายและการบริหาร ตลอดจนถึงความสามารถที่ได้จากการสั่งสมประสบการณ์
                                เพื่อเสนอแนวทางในการเสนอแนวทางแก้ปัญหา 
                                โดยอาศัยวิธีการดำเนินการพัฒนาอย่างเป็นระบบ
                            </p>    --%>                        
						</div>
					</div>				
				</div>
				
				<div class="home-concept">
					<div class="container">
				
						<div class="row center">
							<span class="sun"></span>
							<span class="cloud"></span>
							<div class="col-md-2 col-md-offset-1">
								<div class="process-image" data-appear-animation="bounceIn">
									<img src="img/home-concept-item-1.png" alt="" />
									<strong>Learning</strong>
								</div>
							</div>
							<div class="col-md-2">
								<div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="200">
									<img src="img/home-concept-item-2.png" alt="" />
									<strong>Planning</strong>
								</div>
							</div>
							<div class="col-md-2">
								<div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="400">
									<img src="img/home-concept-item-3.png" alt="" />
									<strong>Building</strong>
								</div>
							</div>
							<div class="col-md-4 col-md-offset-1">
								<div class="project-image">
									<div id="fcSlideshow" class="fc-slideshow">
										<ul class="fc-slides">
											<%--<li><a href="portfolio-single-project.html"><img class="img-responsive" src="img/Nuke1.png" /></a></li>--%>
											<li><a href="portfolio-single-project.html"><img class="img-responsive" src="img/Success1.png" /></a></li>
											<li><a href="portfolio-single-project.html"><img class="img-responsive" src="img/Success2.png" /></a></li>
										</ul>
									</div>
									<strong class="our-work">Destination</strong>
								</div>
							</div>
						</div>
				
					</div>
				</div>
				
				<div class="container">
				
					<div class="row">
						<hr class="tall" />
					</div>
				
				</div>
				
				<div class="container">
				
					<div class="row">
						<div class="col-md-8">
							<h2>ทำไมเราถึง <strong>ต่าง</strong></h2>
							<div class="row">
								<div class="col-sm-6">
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-briefcase"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">เราเข้าใจธุรกิจของคุณ</h4>
											<p class="tall">ทีมงานที่ปรึกษาด้าน Logistic และนักวิเคราะห์ระบบ เราจึงสามารถทำความเข้าใจระบบงานของคุณเพื่อวิเคราะห์และออกแบบระบบได้อย่างมีประสิทธิภาพ ตรงกับความต้องการของคุณมากที่สุด</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-graduation-cap"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">ประสบการณ์ทีมงาน</h4>
											<p class="tall">นักวิเคระาห์ระบบประสบการณ์กว่า 17 ปี ผ่านมาแล้วกว่า 300 โครงการ รวมถึงผู้เชี่ยวชาญด้านโลจิสติกระดับแนวหน้าของเมืองไทย</p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-star"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">คุณภาพสำคัญที่สุด</h4>
											<p class="tall">เก็บทุกรายละเอียดตั้งแต่การวิเคราะห์ออกแบบ ทดสอบการใช้งานในทุกด้านรวมถึงประสิทธิภาพระบบก่อนส่งมอบ เพื่อให้ได้ระบบที่ดีที่สุด</p>
										</div>
									</div>                                    
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-gamepad"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">ทำงานง่ายกว่าที่เคย</h4>
											<p class="tall">คุณจะพบว่าการทำงานผ่านซอฟท์แวร์รูปแบบใหม่ๆที่เรานำเสนอ ไม่ได้ยุ่งยากซับซ้อนเหมือนอย่างเคย ในทางตรงข้ามกลับทำให้คุณรู้สึกสนุกกับการทำงานมากขึ้น</p>
										</div>
									</div>                                   
								</div>
								<div class="col-sm-6">
                                    <div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-graduation-cap"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">บริหารโครงการอย่างเหมาะสม</h4>
											<p class="tall">เรามีวิธีบริหารโครงการที่หลากหลาย เพื่อรองรับโครงการที่มีปัจจัยความเสี่ยงและความเป็นไปได้ที่แตกต่างกัน จึงทำให้เราสามารถรองรับลูกค้าได้ทุกระดับ</p>
										</div>
									</div>                                    
                                    <div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-cloud-download"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">Update ให้คุณทันสมัยอยู่ตลอด</h4>
											<p class="tall">เราเลือกใช้เครื่องมือพัฒนาที่มีอนาคต นั่นหมายถึงทำให้คุณสามารถเพิ่มความสามารถใหม่ๆให้ระบบงานของคุณได้ตลอดเวลา และทันสมัยอยู่เสมอ</p>
										</div>
									</div>
                                    <div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-fighter-jet"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">รีโมทซัพพอร์ตทันที</h4>
											<p class="tall">ในกรณีเกิดปัญหาในการใช้งาน เมื่อคุณแจ้งเรา เราสามารถรีโมตเข้าไปช่วยเหลือและแก้ปัญหาให้คุณได้ทันที ตลอด 24 ชั่วโมง</p>
										</div>
									</div>                                    
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<h2>ผลิตภัณฑ์และบริการ</h2>
				
							<div class="panel-group" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
												<i class="fa fa-cubes"></i>
												SOFTWARE DEVELOPMENT
											</a>
										</h4>
									</div>
									<div id="collapseOne" class="accordion-body collapse in">
										<div class="panel-body">                                            
											    <i class="fa fa-mortar-board"></i> ให้คำแนะนำในการนำ Software มาใช้ในธุรกิจ<br>
	                                            <i class="fa fa-line-chart"></i> พัฒนา Software สำหรับเพิ่มศักยภาพทางธุรกิจ<br>
	                                            <i class="fa fa-database"></i> แก้ไขและปรับปรุงประสิทธิภาพระบบฐานข้อมูลที่มีอยู่<br>
	                                            <i class="fa fa-mortar-board"></i> ให้คำปรึกษาในการบริหารโครงการสารสนเทศภาครัฐ                                       
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
												<i class="fa fa-desktop"></i>
												ENTERPRISE MONITORING TOOLS
											</a>
										</h4>
									</div>
									<div id="collapseTwo" class="accordion-body collapse">
										<div class="panel-body">
										<i class="fa fa-cogs"></i> Enterprise Resource Planning(ERP)<br>
                                        <i class="fa fa-pie-chart"></i> Business Intelligence(BI) and Dashboard<br>
	                                    <i class="fa fa-video-camera"></i> Intellgence Camera & Image Processing<br>                 
										</div>
									</div>
								</div>								
                                <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
												<i class="fa fa-globe"></i>
												GIS DEVELOPMENT
											</a>
										</h4>
									</div>
									<div id="collapseFour" class="accordion-body collapse">
										<div class="panel-body">
												<i class="fa fa-mortar-board"></i> บรรยายเกี่ยวกับระบบ GIS พื้นฐานจนถึงขั้นสูง<br>
	                                            <i class="fa fa-mortar-board"></i> ให้คำปรึกษาในการนำ GIS มาใช้ในธุรกิจ<br>
	                                            <i class="fa fa-line-chart"></i> พัฒนา GIS Software เพิ่มขีดความสามารถทางธุรกิจ<br>
	                                            <i class="fa fa-photo"></i> รับทำแผนที่ จากภาพถ่ายดาวเทียม, ระวางที่ดิน
										</div>
									</div>
								</div>
                                <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
												<i class="fa fa-globe"></i>
												GIS PRODUCT : CADCORP SIS
											</a>
										</h4>
									</div> 
									<div id="collapseFive" class="accordion-body collapse">
										<div class="panel-body">
												<i class="fa fa-copyright"></i> Map Express (Free GIS Software)<br>
	                                            <i class="fa fa-copyright"></i> Desktop GIS<br>
	                                            <i class="fa fa-copyright"></i> GeognoSIS.NET<br>
	                                            <i class="fa fa-copyright"></i> Developer Tools
										</div>
									</div>
								</div>
                                <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
												<i class="fa fa-truck"></i>
												LOGISTICS CONSULTING
											</a>
										</h4>
									</div>
									<div id="collapseSix" class="accordion-body collapse">
										<div class="panel-body">
											<i class="fa fa-mortar-board"></i> บริการที่ปรึกษา Logistics สำหรับองค์กรธุรกิจ
										</div>
									</div>
								</div>
                                <div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
												<i class="fa fa-wrench"></i>
												Machinery Disassembling and Material Supply
											</a>
										</h4>
									</div>
									<div id="collapseSeven" class="accordion-body collapse">
										<div class="panel-body">
											<i class="fa fa-plane"></i> Aircraft Disassembling and Material Supply
										</div>
									</div>
								</div>
                                
							</div>
						</div>
					</div>
				
					<hr class="tall" />
				
					<div class="row center">
						<div class="col-md-12">
							<h2 class="short word-rotator-title">
								ลูกค้าและพันธมิตรที่ร่วมงานกับเรา
								<%--<strong>
									<span class="word-rotate" data-plugin-options='{"delay": 3500, "animDelay": 400}'>
										<span class="word-rotate-items">
											<span>excited</span>
											<span>happy</span>
										</span>
									</span>
								</strong>
								about Porto Template...--%>
							</h2>
							<h4 class="lead tall">ตลอดระยะเวลาที่ผ่านมา เราได้รับความไว้วางใจและมีโอกาสร่วมงานกับองค์การทางธุรกิจรวมถึงหน่วยงานภาครัฐและเอกชน 
                            ซึ่งปัจจุบันเรายังได้รับความไว้วางใจ เป็นที่ปรึกษาและพัฒนาระบบซอฟท์แวร์สำหรับธุรกิจอย่างต่อเนื่อง                            
                            </h4>
						</div>
					</div>
					<div class="row center">
						<div class="owl-carousel" data-plugin-options='{"items": 6, "autoplay": true, "autoplayTimeout": 3000}'>
							<div>
                                <a href="http://www.gpstech.co.th" target="_blank">
								    <img class="img-responsive" src="img/Logo/GPSTech.png" alt="บริษัท จีพีเอสเทค จำกัด" title="บริษัท จีพีเอสเทค จำกัด"  />
                                </a>
							</div>
							<div>
                                <a href="http://www.isobar.com/" target="_blank">
								    <img class="img-responsive" src="img/Logo/Isobar.png" alt="ISOBAR" title="ISOBAR" />
                                </a>
							</div>
                            <div>
								<a href="https://th-th.facebook.com/9mook" target="_blank">
                                    <img class="img-responsive" src="img/Logo/KMK.jpg" alt="บริษัท เก้ามุกข์ จำกัด"  title="บริษัท เก้ามุกข์ จำกัด" />
                                </a>
							</div>
                            <div>
								<a href="http://www.bafcothai.com/" target="_blank">
                                    <img class="img-responsive" src="img/Logo/Bafco.png" alt="บริษัท บางกอกเฟรท ฟอร์เวิดเดอร์ จำกัด"  title="บริษัท บางกอกเฟรท ฟอร์เวิดเดอร์ จำกัด" />
                                </a>
							</div>
                            <div>
								<a href="http://www.limmat.co.th/" target="_blank">
                                    <img class="img-responsive" src="img/Logo/Limmat.jpg" alt="บริษัท ลิมแมท(ประเทศไทย) จำกัด"  title="บริษัท ลิมแมท(ประเทศไทย) จำกัด" />
                                </a>
							</div>
                            <div>
								<a href="http://www.pttplc.com/th/Pages/Home.aspx" target="_blank">
                                    <img class="img-responsive" src="img/Logo/PTT.jpg" alt="บริษัท ปตท. จำกัด(มหาชน)"  title="บริษัท ปตท. จำกัด(มหาชน)" />
                                </a>
							</div>
                             <div>
                                <a href="http://www.samartcorp.com/11/index_th.php" target="_blank">                             
								    <img class="img-responsive" src="img/Logo/Samart.png" alt="บริษัท สามารถคอร์ปอเรชั่น จำกัด (มหาชน)"  title="บริษัท สามารถคอร์ปอเรชั่น จำกัด (มหาชน)" />
                                </a>
							</div>
                           <%--  <div>
								<a href="http://smluniform.com/"  target="_blank">
                                    <img class="img-responsive" src="img/Logo/SML.jpg" alt="บริษัท เอส.เอ็ม.แอล.ยูนิฟอร์ม สแตนดาร์ด จำกัด"  title="บริษัท เอส.เอ็ม.แอล.ยูนิฟอร์ม สแตนดาร์ด จำกัด" />
                                </a>
							</div>--%>
                            <div>
                                <a href="http://www.softproject.biz/" target="_blank">                            
								    <img class="img-responsive" src="img/Logo/SoftProject.png" alt="Soft Project Co., Ltd."  title="Soft Project Co., Ltd." />
                                </a>
							</div>
                            <div>
								<a href="http://www.tescolotus.com/home" target="_blank">
                                    <img class="img-responsive" src="img/Logo/Tesco.jpg" alt="Tesco Lotus"  title="Tesco Lotus" />
                                </a>                                
							</div>
                            <div>
                                <a href="http://www.codi.or.th/" target="_blank">
								    <img class="img-responsive" src="img/Logo/CODI.jpg" alt="สถาบันพัฒนาองค์กรชุมชน(องค์การมหาชน)"  title="สถาบันพัฒนาองค์กรชุมชน(องค์การมหาชน)" />
                                </a> 
							</div>
                            <div>
								<a href="http://www.dft.go.th/" target="_blank">
                                    <img class="img-responsive" src="img/Logo/DFT.jpg" alt="กระทรวงพาณิชย์"  title="กระทรวงพาณิชย์" />
                                </a>
							</div>
                            <div>
								<a href="http://www.betimes.biz/coverpage.html" target="_blank">
                                    <img class="img-responsive" src="img/Logo/Betimes.png" alt="บริษัท บีทามส์ โซลูชั่น จำกัด"  title="บริษัท บีทามส์ โซลูชั่น จำกัด" />
                                </a>                                
							</div>
                            <div>
								<a href="http://www.spu.ac.th/" target="_blank">
                                    <img class="img-responsive" src="img/Logo/SPU.png" alt="มหาวิทยาลัยศรีปทุม"  title="มหาวิทยาลัยศรีปทุม" />
                                </a>
							</div>
                            <div>
								<a href="http://www.opm.go.th/opmportal/index.asp" target="_blank">
                                    <img class="img-responsive" src="img/Logo/OPM.jpg" alt="สํานักงานปลัดสํานักนายกรัฐมนตรี"  title="สํานักงานปลัดสํานักนายกรัฐมนตรี" />
                                </a>
							</div>
                            <div>
								<a href="http://www.blooddonationthai.com/" target="_blank">
                                    <img class="img-responsive" src="img/Logo/RedCross.png" alt="ศูนย์บริการโลหิตแห่งชาติ สภากาชาดไทย"  title="ศูนย์บริการโลหิตแห่งชาติ สภากาชาดไทย" />
                                </a>
							</div>
                            <div>
								<a href="http://www.thaitobacco.or.th/thai/" target="_blank">
                                    <img class="img-responsive" src="img/Logo/TTM.jpg" alt="โรงงานยาสูบ"  title="โรงงานยาสูบ" />
                                </a>                                
							</div>
                            <div>
								<a href="http://www.cadcorp.com/" target="_blank">
                                    <img class="img-responsive" src="img/Logo/Cadcorp.png" alt="CadcorpSIS"  title="CadcorpSIS" />
                                </a>                                
							</div>
                            
						</div>
					</div>
				
				</div>
				
				<%--<div class="map-section">
					<section class="featured footer map">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<div class="recent-posts push-bottom">
										<h2>Latest <strong>Blog</strong> Posts</h2>
										<div class="row">
											<div class="owl-carousel" data-plugin-options='{"items": 1}'>
												<div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
												</div>
												<div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">12</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">11</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
												</div>
												<div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4><a href="blog-post.html">Lorem ipsum dolor</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<h2><strong>What</strong> Client’s Say</h2>
									<div class="row">
										<div class="owl-carousel push-bottom" data-plugin-options='{"items": 1}'>
											<div>
												<div class="col-md-12">
													<blockquote class="testimonial">
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.  Donec hendrerit vehicula est, in consequat.  Donec hendrerit vehicula est, in consequat.</p>
													</blockquote>
													<div class="testimonial-arrow-down"></div>
													<div class="testimonial-author">
														<div class="img-thumbnail img-thumbnail-small">
															<img src="img/clients/client-1.jpg" alt="">
														</div>
														<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
													</div>
												</div>
											</div>
											<div>
												<div class="col-md-12">
													<blockquote class="testimonial">
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
													</blockquote>
													<div class="testimonial-arrow-down"></div>
													<div class="testimonial-author">
														<div class="img-thumbnail img-thumbnail-small">
															<img src="img/clients/client-1.jpg" alt="">
														</div>
														<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">
<!-- Specific Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js" language="javascript" type="text/javascript"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js" language="javascript" type="text/javascript"></script>
<script src="vendor/circle-flip-slideshow/js/jquery.flipshow.js" language="javascript" type="text/javascript"></script>
<script src="js/views/view.home.js" language="javascript" type="text/javascript"></script>

</asp:Content>
