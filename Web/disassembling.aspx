﻿<%@ Page Title="Thai Imagination Technology. Used Aircraft Aircraft Disassembling to extract Aluminium and Titanium Supply from Thailand " Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="disassembling.aspx.vb" Inherits="disassembling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 
 <meta name="keywords" content="Used Aircraft, Aircraft Disassembling, Aircraft Seperate, Aluminium, Aluminum, Titanium,Aluminum and Titanium Supply from Thailand, SOFTWARE DEVELOPMENT, Database Development" />
 <meta name="description" content="Used Aircraft Aircraft Disassembling to extract Aluminium and Titanium Supply from Thailand by Thai Imagination Technology Company" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

                <section class="page-top">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="breadcrumb">									
									<li class="active">Machinery Disassembling and Material Supply</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h1>Aircraft Disassembling and Material Supply</h1>
							</div>
						</div>
					</div>
				</section>

              <div class="container">

					<%--<div class="portfolio-title">
						<div class="row">
							<div class="portfolio-nav-all col-md-1">
								<a href="portfolio-single-project.html" data-tooltip data-original-title="Back to list"><i class="fa fa-th"></i></a>
							</div>
							<div class="col-md-10 center">
								<h2 class="shorter">Lorem Ipsum Dolor</h2>
							</div>
							<div class="portfolio-nav col-md-1">
								<a href="portfolio-single-project.html" class="portfolio-nav-prev" data-tooltip data-original-title="Previous"><i class="fa fa-chevron-left"></i></a>
								<a href="portfolio-single-project.html" class="portfolio-nav-next" data-tooltip data-original-title="Next"><i class="fa fa-chevron-right"></i></a>
							</div>
						</div>
					</div>

					<hr class="tall">--%>

					<div class="row">
						<div class="col-md-4">

							<div class="owl-carousel" data-plugin-options='{"items": 1}'>
								<div>
									<div class="thumbnail">
										<img alt="" class="img-responsive" src="img/disassembling/Boeing747_2.png">
									</div>
								</div>
                                <div>
									<div class="thumbnail">
										<img alt="" class="img-responsive" src="img/disassembling/Boeing747_1.png">
									</div>
								</div>
								<div>
									<div class="thumbnail">
										<img alt="" class="img-responsive" src="img/disassembling/Boeing747_3.png">
									</div>
								</div>
                                <div>
									<div class="thumbnail">
										<img alt="" class="img-responsive" src="img/disassembling/Boeing747_4.png">
									</div>
								</div>
							</div>

						</div>

						<div class="col-md-8">

							<div class="portfolio-info">
								<div class="row">
									<div class="col-md-12 center">
										<ul>
											<li>
												<i class="fa fa-wrench"></i>Disassembling & Material Supply
											</li>											
											<li>
												<i class="fa fa-plane"></i> Aircraft
											</li>
                                            <li>
												<i class="fa fa-calendar"></i> 04 January 2015
											</li>
										</ul>
									</div>
								</div>
							</div>

							<h4>Our <strong>Service</strong></h4>
							<%--<p class="taller">
                                บริการแยกชิ้นส่วนเครื่องจักร และอากาศยานใช้แล้วเพื่อนำไปผลิตหรือเป็นวัตถุดิบใหม่ โดยทีมงานผู้มีประสบการณ์การตัดแยก
                                ซึ่งเรารับประกันปริมาณชิ้นส่วนที่ถอดแยกได้เพื่อให้คุณมั่นใจว่าคุณจะคุ้มค่าในการลงทุน 
                                โดยปัจจุบันบริษัทเป็นตัวแทนให้บริการแยกชิ้นส่วนเครื่องบินเหมาลำให้กับลูกค้าทั้งในและต่างประเทศ และส่งออกชิ้นส่วนโลหะที่ได้ 
                                ทั้งอลูมิเนียมและไททาเนียม เพื่อนำกลับไปแปรรูปและเป็นวัตถุดิบในอุตสาหกรรมต่างๆ
                            </p>  --%>                          
                            <p class="taller">
                            Our company is the <strong>one of few companies has be allowed to serve this service in Thailand</strong>. 
                            We provide the parts to world-wide customer. Many tons of <strong>Aluminium and Titanium</strong> use for re-produce the new material 
                            for many industries.
							</p>
                            <p class="taller">
                            Our service is separate used aircraft parts for producing the new material. 
                            We have experienced machinery disassembling team to <strong>guarantee more than 99.99% yield of production</strong> you will get, 
                            this will make sure you will be well worth for the investment.
                            </p>
                            <h4>For <strong>Boeing 747-300</strong></h4>
                            <p class="taller">
                            When we disassempling Boeing 747-300 and we will get about <strong>120 tons of material parts/one airplane</strong> (not included jet engines, their accessories, electronic components and tires).
                            And the 120 tons of material you can <strong>extract to 70% of aluminium and 30% of titanium</strong>.
							</p>
                            <h4>Our <strong>price</strong></h4>
                            <ul class="list list-skills icons list-unstyled ">
									<li><i class="fa fa-check-circle"></i><strong>$535,000 /one airplane</strong></li>
                                       <li><i class="fa fa-check-circle"></i> <strong>Disassempling service included</strong></li>
                                       <li><i class="fa fa-check-circle"></i> <strong>Free shipping !!</strong></li>
							</ul>
							<ul class="portfolio-details">
								<li>
									<h4><strong>How to order:</strong></h4>
									<ul class="list list-skills icons list-unstyled ">
										<li><i class="fa fa-check-circle"></i> Send us order & LOI</li>
                                        <li><i class="fa fa-check-circle"></i> We reply specification & quotation to you</li>
                                        <li><i class="fa fa-check-circle"></i> On-Site Survey (if you needed)</li>
										<li><i class="fa fa-check-circle"></i> You issue ICPO, COD/LC to start working process</li>
										<li><i class="fa fa-check-circle"></i> Processing, seperating parts 30-90 days depend on order quantity</li>
                                        <li><i class="fa fa-check-circle"></i> Parts Shipment (FOB)</li>
									</ul>
								</li>								
							</ul>
                            <a href="contact.aspx" class="btn btn-primary btn-icon"><i class="fa fa-envelope"></i>Contact us</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
                        </div>
					</div>

					<hr class="tall" />

					<div class="row" data-plugin-options='{"delegate": "a", "type": "image", "gallery": {"enabled": true}}' data-plugin-lightbox="">

						<div class="col-md-12">
							<h3>Recent <strong>Work</strong></h3>
						</div>

						<ul class="portfolio-list">
							<li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_1.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_1.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>
							<li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_2.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_2.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>
                            <li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_3.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_3.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>
                            <li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_4.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_4.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>
                            <li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_5.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_5.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>
                            <li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_6.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_6.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>
                            <li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_7.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_7.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>
                            <li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_8.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_8.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>
                            <li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_9.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_9.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>
                            <li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_10.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_10.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>
                            <li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_11.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_11.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>
                            <li class="col-md-3 col-xs-6">
								<div class="portfolio-item thumbnail">
									<a href="img/disassembling/Boeing747_12.png" class="thumb-info">
										<img alt="Boeing 747 Disassembling" class="img-responsive" src="img/disassembling/Boeing747_12.png">
										<span class="thumb-info-title">
											<span class="thumb-info-inner">Boeing 747</span>
											<span class="thumb-info-type">Disassembling</span>
										</span>
										<span class="thumb-info-action">
											<span title="View" class="thumb-info-action-icon"><i class="fa fa-search"></i></span>
										</span>
									</a>
								</div>
							</li>

						</ul>

					</div>

				</div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">
</asp:Content>

