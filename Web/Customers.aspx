﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Customers.aspx.cs" Inherits="Customers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

.table-bordered {
	border: 1px solid #ddd;
}
.table {
	width: 100%;
	max-width: 100%;
	margin-bottom: 20px;
}
	.table {
		border-collapse: collapse !important;
	}
	table {
	background-color: transparent;
}
table {
	border-spacing: 0;
	border-collapse: collapse;
}
* {
	-webkit-box-sizing: border-box;
		 -moz-box-sizing: border-box;
					box-sizing: border-box;
}
	* {
		color: #000 !important;
		text-shadow: none !important;
		background: transparent !important;
		-webkit-box-shadow: none !important;
						box-shadow: none !important;
	}
	thead {
		display: table-header-group;
	}
	tr {
		page-break-inside: avoid;
	}
	.table > thead:first-child > tr:first-child > th {
	border-top: 0;
}
.table-bordered > thead > tr > th {
	border-bottom-width: 2px;
}
.table-bordered > thead > tr > th {
	border: 1px solid #ddd;
}
.table > thead > tr > th {
	vertical-align: bottom;
	border-bottom: 2px solid #ddd;
}
.table > thead > tr > th {
	padding: 8px;
	line-height: 1.42857143;
	vertical-align: top;
	border-top: 1px solid #ddd;
}
	.table-bordered th {
		border: 1px solid #ddd !important;
	}
	.table th {
		background-color: #fff !important;
	}
	th {
	text-align: left;
}
th {
	padding: 0;
}
.table-bordered > tbody > tr > td {
	border: 1px solid #ddd;
}
.table > tbody > tr > td {
	padding: 8px;
	line-height: 1.42857143;
	vertical-align: top;
	border-top: 1px solid #ddd;
}
	.table-bordered td {
		border: 1px solid #ddd !important;
	}
	.table td {
		background-color: #fff !important;
	}
	td {
	padding: 0;
}
.hidden {
	display: none !important;
	visibility: hidden !important;
}
body a {
	outline: none !important;
}

a {
	color: #0088cc;
}

a {
	color: #CCC;
}

a {
	color: #337ab7;
	text-decoration: none;
}
	a {
		text-decoration: underline;
	}
	a {
	background-color: transparent;
}
.fa {
	display: inline-block;
	font-size: inherit;
	text-rendering: auto;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
            font-style: normal;
            font-variant: normal;
            font-weight: normal;
            line-height: 1;
            font-family: FontAwesome;
            height: 14px;
            width: 4px;
        }
        #addToTable
        {
            width: 187px;
        }
        .style1
        {
            height: 41px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
    <table id="datatable-editable" 
        class="table table-bordered table-striped mb-none">
        <thead>
            <tr>
                <th>
                    ชื่อลูกค้า</th>
                <th>
                    บริษัท</th>
                <th>
                    ที่อยู่</th>
                <th>
                    เบอร์โทร</th>
            </tr>
        </thead>
        <tr class="gradeX">
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeC">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeC">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                &nbsp;</td>
            <td class="style1">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeX">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeC">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeC">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeA">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeX">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeX">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeX">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeC">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <tr class="gradeC">
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="actions">
                <a class="hidden on-editing save-row" href="#"><i class="fa fa-save"></i></a>
                <a class="hidden on-editing cancel-row" href="#"><i class="fa fa-times"></i></a>
                <a class="on-default edit-row" href="#"><i class="fa fa-pencil"></i></a>
                <a class="on-default remove-row" href="#"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        </table>
    <p>
    <button id="addToTable" class="btn btn-primary">
        Add
    </button>
    </p>
</body>
</html>
