﻿
Imports System.Data
Imports System.Data.OleDb

Partial Class wuc_ERPBIMetaTag
    Inherits System.Web.UI.UserControl

    Dim BL As New TIT_BL

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim Desc As String = "บริษัท ไทย อิเมจิเนชั่นเทคโนโลยี จำกัด ที่ปรึกษาการนำซอฟท์แวร์ โปรแกรม ERP, BI และ Dashboard มาใช้เพิ่มประสิทธิภาพการทำงานของธุรกิจ"
            Dim A As Integer = Desc.Length
            Dim ConnStr As String = BL.BuildExcelConnectionString(Server.MapPath("Keyword/ERPBI.xls"), False)
            Dim Keyword As String = ""
            Dim DA As New OleDbDataAdapter("SELECT * FROM [Sheet1$]", ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)

            For i As Integer = 0 To DT.Rows.Count - 1
                If DT.Rows(i)(0).ToString = "" Then Exit For
                Keyword &= DT.Rows(i)(0).ToString & ", "
            Next
            If Keyword <> "" Then Keyword = Keyword.Substring(0, Keyword.Length - 2)
            metaKeyword.Attributes("content") = Keyword
        End If
    End Sub

End Class
