﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_CadcorpMetaTag.ascx.vb" Inherits="wuc_CadcorpMetaTag" %>

<meta name="keywords" id="metaKeyword" runat="server" content="" />
<meta name="description" id="metaDescription" runat="server" content="ตัวแทนจำหน่ายซอฟท์แวร์ระบบสารสนเทศภูมิศาสตร์ GIS จาก CadcorpSIS ประเทศอังกฤษ สามารถใช้เปิดไฟล์ข้อมูลแผนที่ได้ทุกนามสกุล ไม่ว่าจะเป็น GIS, CAD, Web Map Server, Database รวมถึงเครื่องมือพัฒนา Solution สำหรับนำข้อมูลแผนที่ GIS ที่คุณมี ไปใช้ประโยชน์ในแอพพลิเคชั่นทางธุรกิจขององค์กร" />