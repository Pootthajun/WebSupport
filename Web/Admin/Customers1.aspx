﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPageTIT.master" AutoEventWireup="false" CodeFile="Customers1.aspx.vb" Inherits="Admin_Customers1" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <!-- Basic -->
    <meta charset="utf-8">
    <title>Shortcodes | Porto - Responsive HTML5 Template 3.7.0</title>
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Porto - Responsive HTML5 Template">
    <meta name="author" content="okler.net">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light"
        rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../vendor/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../vendor/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="../vendor/owlcarousel/owl.carousel.min.css" media="screen">
    <link rel="stylesheet" href="../vendor/owlcarousel/owl.theme.default.min.css" media="screen">
    <link rel="stylesheet" href="../vendor/magnific-popup/magnific-popup.css" media="screen">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="../css/theme.css">
    <link rel="stylesheet" href="../css/theme-elements.css">
    <link rel="stylesheet" href="../css/theme-blog.css">
    <link rel="stylesheet" href="../css/theme-shop.css">
    <link rel="stylesheet" href="../css/theme-animate.css">
    <!-- Skin CSS -->
    <link rel="stylesheet" href="../css/skins/default.css">
    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="../css/custom.css">
    <!-- Head Libs -->
    <script src="../vendor/modernizr/modernizr.js"></script>
    <!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->
    <!--[if lte IE 8]>
			<script src="vendor/respond/respond.js"></script>
			<script src="vendor/excanvas/excanvas.js"></script>
		<![endif]-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .style2
        {
            color: #0066FF;
        }
    </style>
<div>
   <asp:Label ID="Label1" runat="server">
    <h1 class="style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Customers Information</h1>
    </asp:Label>
    </div>
    <div role="main" class="main">
        <div class="container">
            <div class="panel-footer" style="margin-bottom: 10px; max-width: 945px;">
                <div class="panel-body" style="margin-bottom: -15px">
                    <ul class="pager">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                <asp:DropDownList ID="ddyrcmt" runat="server" class="form-control">
                                <asp:ListItem Text="Year" Value="0" disabled selected hidden ></asp:ListItem>
                                <asp:ListItem Text="2001" Value="ddycmt" ></asp:ListItem>
                                <asp:ListItem Text="2002" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2003" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2004" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2005" Value="ddycmt" ></asp:ListItem>
                                <asp:ListItem Text="2006" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2007" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2008" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2009" Value="ddycmt" ></asp:ListItem>
                                <asp:ListItem Text="2010" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2011" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2012" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2013" Value="ddycmt" ></asp:ListItem>
                                <asp:ListItem Text="2014" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2015" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2016" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2017" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2018" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2019" Value="ddycmt"></asp:ListItem>
                                <asp:ListItem Text="2020" Value="ddycmt"></asp:ListItem>
                            </asp:DropDownList>
                                </div>
                                <div class="col-md-2">
                                 <asp:DropDownList ID="DropDownList1" runat="server" class="form-control">
                                <asp:ListItem Text="Month" Value="0" disabled selected hidden ></asp:ListItem>
                                <asp:ListItem Text="01" Value="ddmcmt" ></asp:ListItem>
                                <asp:ListItem Text="02" Value="ddmcmt"></asp:ListItem>
                                <asp:ListItem Text="03" Value="ddmcmt"></asp:ListItem>
                                <asp:ListItem Text="04" Value="ddmcmt"></asp:ListItem>
                                <asp:ListItem Text="05" Value="ddmcmt" ></asp:ListItem>
                                <asp:ListItem Text="06" Value="ddmcmt"></asp:ListItem>
                                <asp:ListItem Text="07" Value="ddmcmt"></asp:ListItem>
                                <asp:ListItem Text="08" Value="ddmcmt"></asp:ListItem>
                                <asp:ListItem Text="09" Value="ddmcmt" ></asp:ListItem>
                                <asp:ListItem Text="10" Value="ddmcmt"></asp:ListItem>
                                <asp:ListItem Text="11" Value="ddmcmt"></asp:ListItem>
                                <asp:ListItem Text="12" Value="ddmcmt"></asp:ListItem>
                                </asp:DropDownList>
                                </div>
                                <div class="col-md-4">
                                    <asp:TextBox ID="TextBox6" placeholder="..." runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtncycmt" placeholder="Name Company" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtascmt" placeholder="Alias" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtcecmt" placeholder="Code" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtidcmt" placeholder="Tax:ID" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#Address" data-toggle="tab"></i> Contact Address</a>
                            </li>
                            <li><a href="#Person" data-toggle="tab">Contact Person</a> </li>
                            <li><a href="#Type" data-toggle="tab">Business Type</a> </li>
                        </ul>
                        <div class="tab-content">
                            <div id="Address" class="tab-pane active">
                                <p>
                                    <div class="panel panel-default">
                                        <div id="collapseOne" class="accordion-body collapse in">
                                            <div class="panel-body">
                                                <form action="" id="" method="post">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label>
                                                                Company Name Thai</label>
                                                            <asp:TextBox ID="txtCnicmt"  runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>
                                                                Company Name English</label>
                                                             <asp:TextBox ID="txtcnhcmt"  runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <label>
                                                                Address Company</label>
                                                                <asp:TextBox class="form-control" ID="txtaccmt" TextMode="multiline" Columns="50"
                                    Rows="3" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>
                                                                District</label>
                                                                 <asp:DropDownList ID="DropDownList2" runat="server" class="form-control">
                                <asp:ListItem Text="District" Value="0" disabled selected hidden ></asp:ListItem>
                                <asp:ListItem Text="**" Value="dd1cmt"></asp:ListItem>
                                <asp:ListItem Text="***" Value="dd2cmt"></asp:ListItem>
                                </asp:DropDownList>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>
                                                                Province</label>
 <asp:DropDownList ID="DropDownList3" runat="server" class="form-control">
                                <asp:ListItem Text="Province" Value="0" disabled selected hidden ></asp:ListItem>
                                <asp:ListItem Text="**" Value="dd4cmt"></asp:ListItem>
                                <asp:ListItem Text="***" Value="dd5cmt"></asp:ListItem>
                                </asp:DropDownList>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>
                                                                Postal_Code</label>
                                                           <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label>
                                                                Phone</label>
                                                            <asp:TextBox ID="txtpecmt" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>
                                                                Fax</label>
                                                            <asp:TextBox ID="txtfxcmt" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label>
                                                                Moble Phone</label>
                                                            <asp:TextBox ID="txtmecmt" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label>
                                                                Email</label>
                                                            <asp:TextBox ID="txtelcmt" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>
                                                                Web</label>
                                                            <asp:TextBox ID="txtwbcmt" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </p>
                            </div>
                            <div id="Person" class="tab-pane">
                                <p>
                                    <div class="panel panel-default">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:Label ID="labellist" runat="server" Text="Label"><h4>List</h4></asp:Label>
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th class="style1">
                                                                #
                                                            </th>
                                                            <th class="style1">
                                                                First Name
                                                            </th>
                                                            <th class="style1">
                                                                Last Name
                                                            </th>
                                                            <th class="style1">
                                                                Position
                                                            </th>
                                                            <th class="style1">
                                                                Phone
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <asp:Repeater ID="Repeater2" runat="server">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lbFirst_Name" runat="server" Text="Label"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Last_Name" runat="server" Text="Label"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Username" runat="server" Text="Label"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Edit" runat="server" Text="Label"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="LinkButton1" runat="server">
                            <i class="fa fa-pencil"></i>
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server">
                            <i class="fa fa-times"></i> fa-times
                                                                    </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" style="margin-bottom: 15px; text-align: center">
                                                <asp:Button ID="btncnaddcmt" class="btn btn-primary" runat="server" Text="Add.." />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div id="Div1" class="accordion-body collapse in">
                                            <div class="panel-body">
                                                <form action="" id="Form1" method="post">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label>
                                                                First Name1</label>
                                                            <asp:TextBox ID="txtf1cmt" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>
                                                                Last Name</label>
                                                            <a title="Remove this item" class="remove" style="margin-left: 340px;" href="#"><i
                                                                class="fa fa-times"></i></a>
                                                           <asp:TextBox ID="txtlecmt" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <label>
                                                                Position</label>
                                                            <asp:TextBox ID="txtpncmt" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>
                                                                Section</label>
                                                            <asp:TextBox ID="txtsncmt" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label>
                                                                Phone</label>
                                                            <asp:TextBox ID="TextBox2" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>
                                                                Phone1</label>
                                                            <asp:TextBox ID="TextBox3" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>
                                                                Email</label>
                                                            <asp:TextBox ID="TextBox4" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>
                                                                Email1</label>
                                                            <asp:TextBox ID="TextBox7" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="text-align: right;">
                                                    <div class="col-md-12" style="margin-top: 10px;">
                                                        <asp:Button ID="btncnsavecmt" class="btn btn-primary" runat="server" Text="Save.. " />
                                                    </div>
                                                </div>
                                                
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </p>
                            </div>
                            <div id="Type" class="tab-pane">
                                <p>
                                    <div class="panel panel-default">
                                        <div id="Type1" class="accordion-body collapse in">
                                            <div class="panel-body">
                                                <form action="" id="Type3" method="post">
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        <asp:TextBox ID="txtbtecmt" placeholder="Business Type" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                       
                                            <asp:LinkButton ID="btnremoveweadmin" class="fa fa-times" style="margin-top:10px"  runat="server"></asp:LinkButton>
             
                                                    <div class="row" style="text-align: center;">
                                                        <div class="col-md-12" style="margin-top: 10px;">
                                                            <asp:Button ID="btnbteaddcms" class="btn btn-primary" runat="server" Text="Add.." />
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </p>
                            </div>
                            <div class="row">
                <div class="col-md-12">
                    <div class="row" style="text-align: center; margin-bottom:-5px; margin-top:-20px">
                        <div class="col-md-12" style="margin-top: 10px;">
                            <asp:Button ID="btnsavecascpnbtpcmt" class="btn btn-primary" style="padding:10px" runat="server" Text="Save..." />
                        </div>
                    </div>
                </div>
            </div>
                        </div>
                    </div>
                </div>
            </div>
               <div class="control-group">
											<label class="control-label">Black List ตั้งแต่</label>
											<div class="controls">																
												<asp:TextBox ID="txtEdit_Start" runat="server" CssClass="medium m-wrap" Style="text-align:center;"></asp:TextBox>
                                                <ajax:CalendarExtender ID="cal_txtStart" runat="server" TargetControlID="txtEdit_Start" Format="dd MMM yyyy"></ajax:CalendarExtender>
											</div>
										</div>      
        </div>
    </div>
    <!-- Vendor -->
    <script src="../vendor/jquery/jquery.js"></script>
    <script src="../vendor/jquery.appear/jquery.appear.js"></script>
    <script src="../vendor/jquery.easing/jquery.easing.js"></script>
    <script src="../vendor/jquery-cookie/jquery-cookie.js"></script>
    <script src="../vendor/bootstrap/bootstrap.js"></script>
    <script src="../vendor/common/common.js"></script>
    <script src="../vendor/jquery.validation/jquery.validation.js"></script>
    <script src="../vendor/jquery.stellar/jquery.stellar.js"></script>
    <script src="../vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../vendor/jquery.gmap/jquery.gmap.js"></script>
    <script src="../vendor/isotope/jquery.isotope.js"></script>
    <script src="../vendor/owlcarousel/owl.carousel.js"></script>
    <script src="../vendor/jflickrfeed/jflickrfeed.js"></script>
    <script src="../vendor/magnific-popup/jquery.magnific-popup.js"></script>
    <script src="../vendor/vide/vide.js"></script>
    <!-- Theme Base, Components and Settings -->
    <script src="js/theme.js"></script>
    <!-- Theme Custom -->
    <script src="js/custom.js"></script>
    <!-- Theme Initialization Files -->
    <script src="js/theme.init.js"></script>
    <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script type="text/javascript">
		
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-12345678-1']);
			_gaq.push(['_trackPageview']);
		
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		
		</script>
		 -->

</asp:Content>


