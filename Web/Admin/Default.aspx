﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPageTIT.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Admin_Default" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="labellist" runat="server" Text="Label"><h4>List</h4></asp:Label>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TITConnectionString %>" 
        OldValuesParameterFormatString="original_{0}" 
        SelectCommand="SELECT * FROM [TB_Staff]">
    </asp:SqlDataSource>
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th class="style1">
                                                                #
                                                            </th>
                                                            <th class="style1">
                                                                ID
                                                            </th>
                                                            <th class="style1">
                                                                Name
                                                            </th>
                                                            <th class="style1">
                                                                Gender
                                                            </th>
                                                            <th class="style1">
                                                                Type ID
                                                            </th>
                                                            <th class="style1">
                                                                Year
                                                            </th>
                                                            <th class="style1">
                                                                Month
                                                            </th>
                                                            <th class="style1">
                                                                Running_No
                                                            </th>
                                                            <th class="style1">
                                                                ID Cards
                                                            </th>
                                                            <th class="style1">
                                                                Mobile1
                                                            </th>
                                                            <th class="style1">
                                                                Mobile2
                                                            </th>
                                                             <th class="style1">
                                                                EmailTIT
                                                            </th>
                                                             <th class="style1">
                                                                EmailOther
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <%--<asp:Repeater ID="Repeater1" runat="server">
                                                        <ItemTemplate>--%>
                                                      <tbody >
                                                      <tr>
                                                            <td>
                                                                #
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="ID" runat="server" Text="ID"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Name" runat="server" Text="Name"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Gender" runat="server" Text="Gender"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="TYPE_ID" runat="server" Text="TypeID"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Year" runat="server" Text="Year"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Month" runat="server" Text="Month"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Running_No" runat="server" Text="Running_No"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="ID_Cards" runat="server" Text="ID Cards"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Mobile_1" runat="server" Text="Mobile1"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Mobile_2" runat="server" Text="Mobile2"></asp:Label>
                                                            </td>
                                                             <td>
                                                                <asp:Label ID="Email_TIT" runat="server" Text="EmailTIT"></asp:Label>
                                                            </td>
                                                             <td>
                                                                <asp:Label ID="Email_Other" runat="server" Text="EmailOther"></asp:Label>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                #
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Label1" runat="server" Text="ID"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Label2" runat="server" Text="Name"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Label3" runat="server" Text="Gender"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Label4" runat="server" Text="TypeID"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Label5" runat="server" Text="Year"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label6" runat="server" Text="Month"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label7" runat="server" Text="Running_No"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label8" runat="server" Text="ID Cards"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label9" runat="server" Text="Mobile1"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label10" runat="server" Text="Mobile2"></asp:Label>
                                                            </td>
                                                             <td>
                                                                <asp:Label ID="Label11" runat="server" Text="EmailTIT"></asp:Label>
                                                            </td>
                                                             <td>
                                                                <asp:Label ID="Label12" runat="server" Text="EmailOther"></asp:Label>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                #
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Label13" runat="server" Text="ID"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Label14" runat="server" Text="Name"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Label15" runat="server" Text="Gender"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Label16" runat="server" Text="TypeID"></asp:Label>
                                                            </td>
                                                            <td>
                                                             <asp:Label ID="Label17" runat="server" Text="Year"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label18" runat="server" Text="Month"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label19" runat="server" Text="Running_No"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label20" runat="server" Text="ID Cards"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label21" runat="server" Text="Mobile1"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label22" runat="server" Text="Mobile2"></asp:Label>
                                                            </td>
                                                             <td>
                                                                <asp:Label ID="Label23" runat="server" Text="EmailTIT"></asp:Label>
                                                            </td>
                                                             <td>
                                                                <asp:Label ID="Label24" runat="server" Text="EmailOther"></asp:Label>
                                                            </td>

                                                        </tr>
                                                        </tbody>
                                                         
                                                        <%--</ItemTemplate>
                                                    </asp:Repeater>--%>
                                                   
                                                </table>
    <br />
    <br />
    <br />
    <br />
</asp:Content>

