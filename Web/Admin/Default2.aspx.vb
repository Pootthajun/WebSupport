﻿Imports System.Data
Imports System.Data.SqlClient
Imports System
Imports Microsoft.VisualBasic


Partial Class Admin_Default2
    Inherits System.Web.UI.Page


    'Dim BL As New AVLBL
    Dim GL As New GenericLib

    Private ReadOnly Property PageName As String
        Get
            Return "AssessmentView.aspx"
        End Get
    End Property

    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    'Private Property AccessMode As AVLBL.AccessRole
    '    Get
    '        Try
    '            Return ViewState("AccessMode")
    '        Catch ex As Exception
    '            Return AVLBL.AccessRole.None
    '        End Try
    '    End Get
    '    Set(ByVal value As AVLBL.AccessRole)
    '        ViewState("AccessMode") = value
    '    End Set
    'End Property

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    'End Sub

    ''Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
    ''    Me.pnlForm.Visible = False

    ''    If Me.fUpload.HasFile = False Or Me.txtName.Text = "" Then
    ''        Me.lblStatus.Text = "Please input Name and Chooes File."
    ''    Else

    ''        *** Read Binary Data ***'
    ''        Dim imbByte(fUpload.PostedFile.InputStream.Length) As Byte
    ''        fUpload.PostedFile.InputStream.Read(imbByte, 0, imbByte.Length)

    ''        *** MimeType ***'
    ''        Dim ExtType As String = System.IO.Path.GetExtension(fUpload.PostedFile.FileName).ToLower()
    ''        Dim strMIME As String = Nothing
    ''        Select Case ExtType
    ''            Case ".gif"
    ''                strMIME = "image/gif"
    ''            Case ".jpg", ".jpeg", ".jpe"
    ''                strMIME = "image/jpeg"
    ''            Case ".png"
    ''                strMIME = "image/png"
    ''            Case Else
    ''                Me.lblStatus.Text = "Invalid file type."
    ''                Exit Sub
    ''        End Select

    ''        *** Insert to Database ***'
    ''        Dim objConn As New SqlConnection
    ''        Dim strConnString, strSQL As String

    ''        strConnString = "Server=localhost;UID=sa;PASSWORD=;database=mydatabase;Max Pool Size=400;Connect Timeout=600;"

    ''        strSQL = "INSERT INTO files (Name,FilesName,FilesType) " & _
    ''        " VALUES " & _
    ''        " (@sName,@sFilesName,@sFilesType)"
    ''        objConn.ConnectionString = strConnString
    ''        objConn.Open()

    ''        Dim objCmd As New SqlCommand(strSQL, objConn)
    ''        objCmd.Parameters.Add("@sName", SqlDbType.VarChar).Value = Me.txtName.Text
    ''        objCmd.Parameters.Add("@sFilesName", SqlDbType.Binary).Value = imbByte
    ''        objCmd.Parameters.Add("@sFilesType", SqlDbType.VarChar).Value = strMIME
    ''        objCmd.ExecuteNonQuery()

    ''        objConn.Close()
    ''        objConn = Nothing

    ''        Me.lblStatus.Text = "File Upload Successfully. Click <a href='ListPicture.aspx'>here</a> to view."
    ''    End If
    ''End Sub

    'Dim LastDept As String = ""
    'Protected Sub rptList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
    '    If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

    '    Dim trDept As HtmlTableRow = e.Item.FindControl("trDept")
    '    Dim lblDept As Label = e.Item.FindControl("lblDept")
    '    Dim lblDocType As Label = e.Item.FindControl("lblDocType")
    '    Dim lnkRef As HtmlAnchor = e.Item.FindControl("lnkRef")
    '    Dim lblSup As Label = e.Item.FindControl("lblSup")
    '    'Dim lblTax As Label = e.Item.FindControl("lblTax")
    '    Dim lblItem As Label = e.Item.FindControl("lblItem")
    '    Dim lblStep As Label = e.Item.FindControl("lblStep")
    '    Dim lblUpdate As Label = e.Item.FindControl("lblUpdate")
    '    Dim lblResult As Label = e.Item.FindControl("lblResult")
    '    Dim lblAVL As Label = e.Item.FindControl("lblAVL")

    '    If LastDept <> e.Item.DataItem("Dept_Name").ToString Then
    '        LastDept = e.Item.DataItem("Dept_Name").ToString
    '        lblDept.Text = LastDept
    '        trDept.Visible = True
    '    Else
    '        trDept.Visible = False
    '    End If
    '    lblDept.Attributes("Dept_ID") = e.Item.DataItem("Dept_ID")
    '    If IsDBNull(e.Item.DataItem("ReAss_ID")) Then
    '        lblDocType.Text = "ประเมินผู้ขายใหม่"
    '        lblDocType.ForeColor = Drawing.Color.Teal
    '        lnkRef.InnerHtml = e.Item.DataItem("Ass_Ref_Code").ToString
    '        lnkRef.Style("color") = "Teal"
    '        lnkRef.HRef = "Assessment_Edit.aspx?Ass_ID=" & e.Item.DataItem("Ass_ID") & "&From=Ass"
    '    Else
    '        lblDocType.Text = "ประเมินทบทวน"
    '        lblDocType.ForeColor = Drawing.Color.Blue
    '        lnkRef.InnerHtml = e.Item.DataItem("ReAss_Ref_Code").ToString
    '        lnkRef.Style("color") = "Blue"
    '        lnkRef.HRef = "Reassessment_Edit.aspx?Ass_ID=" & e.Item.DataItem("Ass_ID") & "&ReAss_ID=" & e.Item.DataItem("ReAss_ID") & "&From=ReAss"
    '    End If
    '    lblSup.Text = e.Item.DataItem("S_Name").ToString
    '    'lblTax.Text = e.Item.DataItem("S_Tax_No").ToString
    '    lblItem.Text = "(" & e.Item.DataItem("Item_Code").ToString & ") " & e.Item.DataItem("Item_Name").ToString
    '    lblStep.Text = e.Item.DataItem("Step_TH").ToString
    '    lblStep.ForeColor = BL.GetStepColor(e.Item.DataItem("Current_Step"))
    '    lblUpdate.Text = GL.ReportThaiDate(e.Item.DataItem("Update_Time"))
    '    BL.DisplayAssessmentGridResultLabel(e.Item.DataItem("Get_Score"), e.Item.DataItem("Pass_Score"), e.Item.DataItem("Max_Score"), e.Item.DataItem("Pass_Status"), lblResult)
    '    lblAVL.Text = e.Item.DataItem("AVL_Code").ToString
    'End Sub
End Class
