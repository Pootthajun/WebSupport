﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="about_vision.aspx.vb" Inherits="about_vision" %>

<%@ Register src="wuc_HeadMetaTag.ascx" tagname="wuc_HeadMetaTag" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<uc1:wuc_HeadMetaTag ID="wuc_HeadMetaTag1" runat="server" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

<div role="main" class="main">

				<section class="page-top">
					<div class="row">
				            <div class="col-md-12">
					            <ul class="breadcrumb">									
						            <li class="active">เกี่ยวกับเรา</li>
					            </ul>
				            </div>
			            </div>	
                    <div class="container">						
						<div class="row">
							<div class="col-md-12">
								<h1>ประวัติบริษัท</h1>
							</div>
						</div>
					</div>
				</section>

				<div class="container">
					<div class="row center">
						<div class="col-md-12">
							<h2 class="short word-rotator-title">
                                 พัฒนาเครื่องมือในการทำงาน เพื่อช่วยให้คุณ
                                 <strong class="inverted" data-appear-animation="bounceIn">
									<span class="word-rotate" data-plugin-options='{"delay": 2000}'>
										<span class="word-rotate-items">
											<span>ลดต้นทุน</span>
                                            <span>ทำงานเป็นระบบ</span>
											<span>ลดความซ้ำซ้อน</span>
											<span>ทำงานง่ายขึ้น</span>
                                            <span>มองเห็นภาพรวมธุรกิจ</span>
                                            <span>มองเห็นแนวโน้มที่จะเกิดขึ้น</span>
                                            <span>เปิดช่องทางใหม่ๆ</span>
                                            <span>ขยายฐานลูกค้า</span>
										</span>
									</span>
								</strong>
							</h2>
							<p class="featured lead">
								ปัจจุบันบริษัท ไทย อิเมจิเนชั่นเทคโนโลยี จำกัด ดำเนินธุรกิจด้านเทคโนโลยีและการพัฒนาซอฟท์แวร์เพื่อใช้ในธุรกิจมาเป็นปีที่ 3 ซึ่งลูกค้าของเราประกอบด้วยธุรกิจทุกระดับ ตั้งแต่ร้านค้าปลีก บริษัท SME หน่วยงานราชการ รัฐวิสาหกิจ ไปจนถึงยักษ์ใหญ่ในวงการอุตสาหกรรมและพลังงาน 
                                ครอบคลุมถึงองค์กรธุรกิจในต่างประเทศ 
							</p>
                            <p class="featured lead">
                                โดยแนวคิดของบริษัทฯ ยึดถือแนวทางการพัฒนาเทคโนโลยีและซอฟท์แวร์โดยวิธีใช้เครื่องมือที่หลากหลาย 
                                มาพัฒนาเครื่องมือเฉพาะ เพื่อให้<strong>เหมาะสำหรับธุรกิจของลูกค้าแต่ละราย</strong> เพราะเราเชื่อว่าลูกค้าแต่ละรายมีวิธีการดำเนินธุรกิจที่แตกต่างกัน ถึงแม้ว่าเป็นธุรกิจประเภทเดียวกันก็ตาม
                            </p>
						</div>
					</div>

					<hr class="tall" />
				</div>

				<div class="container">
                   <div class="row center">
                        <h1 class="short word-rotator-title">
                        ผลิตภัณฑ์และการให้บริการ
                        </h1>
                   </div>

					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-4">
									<div class="feature-box secundary">
										<div class="feature-box-icon">
											<i class="fa fa-cubes"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">การพัฒนาซอฟท์แวร์</h4>
											<p class="tall"><span class="alternative-font">17 ปี ประสบการณ์เกว่า 300 โครงการ</span>
                                            ที่เราพัฒนาให้กับลูกค้าหลากธุรกิจ ไม่ว่าจะเป็นบริษัท SME ธุรกิจอุตสาหกรรมและพลังงาน ระบบงานรัฐวิสาหกิจ โครงการภาครัฐ 
                                            รวมไปถึงร้านค้าปลีก 
                                            ด้วยความเชี่ยวชาญในการวิเคราะห์และออกแบบระบบ รวมถึงการศึกษาและนำเทคโนโลยีใหม่ๆมาใช้อยู่เสมอ
                                            อีกทั้งเรายังมองระบบในมุมของลูกค้าเป็นสำคัญ
                                            ทำให้เราสามารถพัฒนาระบบที่ครอบคลุม ทันสมัย และใช้งานได้กว่าง่ายที่เคย เพื่อให้ตรงความต้องการสำหรับของคุณมากที่สุด 
                                            นอกเหนือจากนี้หากคุณเกิดคำถาม ลองคลิกเพื่อดู <a href="project_portal.aspx">ตัวอย่างผลงาน</a> </p>
										</div>
									</div>
									<div class="feature-box secundary">
										<div class="feature-box-icon">
											<i class="fa fa-desktop"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">Enterprise Monitoring Tools</h4>
											<p class="tall">
                                            เครื่องมือสำหรับตรวจสอบระบบ ไม่ว่าจะเป็น Dashboard, War Room ระบบกล้องวงจรปิด CCTV เพื่อตรวจจับสถานะและเฝ้าระวังความผิดปกติในองค์กรและธุรกิจของคุณในทุกๆด้าน
                                            โดยเฉพาะอย่างยิ่งเราอยากให้คุณลองสัมผัสกับความอัจฉริยะของ <a href="product_icip.aspx">ICIP</a> (Intellgence Camera & Image Processing) 
                                            ซึ่งคุณจะเปลี่ยนความคิดเดิมๆเกี่ยวกับกล้องวงจรปิดไปเลย เพราะไม่เพียงช่วยตรวจจับความผิดพลาดเท่านั้น แต่ยัง<a href="product_icip.aspx">ช่วยวิเคราะห์กิจกรรมทางธุรกิจให้คุณได้อีกด้วย !!</a>
                                            </p>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="feature-box secundary">
										<div class="feature-box-icon">
											<i class="fa fa-globe"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">การพัฒนาระบบสารสนเทศภูมิศาสตร์ GIS</h4>
											<p class="tall">
                                            ในความเป็นจริงแล้ว ธุรกิจสามารถนำ GIS มาประยุกต์ใช้ประโยชน์ในธุรกิจได้มหาศาล ไม่เพียงแค่ให้ลูกค้ารู้ที่ตั้งของหน่วยงานเราเท่านั้น
                                            แต่ยังใช้ในการวิเคราะห์ได้ทั้งกระบวนการโลจิสติกส์ ไม่ว่าจะเป็นการบริหารเส้นทางขนส่ง การวิเคราะห์พฤติกรรมลูกค้า การประเมินความเสี่ยงในการลงทุน และอีกมาก โดยทีมงานของบริษัท
                                            มีความเชี่ยวชาญและเป็น<a href="product_gis.aspx">วิทยากรบรรยาย</a> ให้กับหลายสถาบัน 
                                            รวมถึงประสบการณ์การพัฒนาซอฟท์แวร์ GIS มาอย่างยาวนาน รวมถึงการให้                                            
                                            <a href="product_digitize.aspx">บริการจัดทำข้อมูลแผนที่</a>                                          
                                            </p>
										</div>
									</div>
									<div class="feature-box secundary">
										<div class="feature-box-icon">
											<i class="fa fa-globe"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">ซอฟท์แวร์ระบบสารสนเทศภูมิศาสตร์ GIS</h4>
											<p class="tall">เราเป็นตัวแทนจำหน่าย <a href="product_cadcorp_DesktopGIS.aspx">CadcorpSIS</a> จากประเทศอังกฤษ 
                                            ด้วยรูปแบบการทำงานที่ง่ายกว่า สะดวกกว่า และความสามารถที่ในการ<a href="product_cadcorp_DesktopGIS.aspx">แปลงข้อมูลแผนที่ GIS ข้าม Format ได้กว่า 150 นามสกุล</a> 
                                            รวมถึงราคาลิขสิทธิ์ที่ต่ำกว่าคู่แข่ง ปัจจุบันจึงใช้งานอยู่ในหน่วยงานราชการเป็นจำนวนมาก และมหาวิทยาลัยหลายแห่ง อีกทั้งยังมี<a href="product_cadcorp_DeveloperTool.aspx">เครื่องมือพัฒนา</a>เพื่อให้
                                            โปรแกรมเมอร์ของคุณสามารถนำไปพัฒนาซอฟท์แวร์เพื่อใช้ในองค์กรของคุณเองได้ตามความต้องการ                                     
                                            </p>
										</div>
									</div>
								</div>
								<div class="col-md-4">									
									<div class="feature-box secundary">
										<div class="feature-box-icon">
											<i class="fa fa-truck"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">ให้บริการที่ปรึกษาโลจิสจิกส์</h4>
											<p class="tall">
                                            ด้วยวิทยากรและทีมงานที่ปรึกษา ระดับแนวหน้าของประเทศ ซึ่งมีประสบการณ์ในการให้คำปรึกษาในการพัฒนาโลจิสติกส์ในองค์กรมาแล้วกว่า 11 ปี
                                            เราสามารถวิเคราะห์และ<a href="logistics.aspx">ให้คำแนะนำในการปรับปรุงการจัดการโลจิสติกส์</a>ในองค์กรของคุณ อีกทั้งยังมีความยินดีให้บริการบรรยายเพื่อเสริมสร้าง
                                            ความรู้ความเข้าใจและแนวคิดที่ดี ในการทำงานให้กับพนักงานของในองค์กรคุณ
                                            </p>
										</div>
									</div>
                                    <div class="feature-box secundary">
										<div class="feature-box-icon">
											<i class="fa fa-wrench"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">บริการแยกชิ้นส่วนเครื่องจักร</h4>
											<p class="tall">
                                            บริการแยกชิ้นส่วนเครื่องจักร เพื่อนำไปผลิตหรือเป็นวัตถุดิบในการผลิตใหม่ โดยเรามีทีมงานผู้มีประสบการณ์ 
                                            ซึ่งเรามีการรับประกันปริมาณชิ้นส่วนที่ถอดแยกได้เพื่อให้คุณมั่นใจว่าคุณจะคุ้มค่าในการลงทุน 
                                            โดยปัจจุบันบริษัทเป็นตัวแทนให้บริการ<a href="disassembling.aspx">แยกชิ้นส่วนเครื่องบินเหมาลำ</a>ลูกค้าทั้งในและต่างประเทศ 
                                            ตลอดจนรับผิดชอบขั้นตอนในการขนส่งโดยบริษัท 
                                            ส่งออกชิ้นส่วนโลหะที่ได้ 
                                            นำกลับไปแปรรูปและเป็นวัตถุดิบในการผลิตใหม่
                                            </p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

				<section class="featured highlight footer" style="margin-bottom:50px;">
					<div class="container">
						<div class="row center counters">
							<div class="col-md-12 col-sm-6">
								<strong data-to="100" data-append="%">0</strong>
								<label>Quality</label>
							</div>							
						</div>
					</div>
				</section>

				

				<div class="container">
					<div class="row">
						<div class="col-md-5">
							<h2><strong>คุณภาพ</strong> คือวิสัยทัศน์</h2>
							<p> 
                            วิสัยทัศน์อย่างเดียวที่เรามีและสำคัญที่สุดคือ "คุณภาพ" และความเข้าใจระบบธุรกิจของลูกค้า 
                            </p>
                            <p>
                            คำว่า Enterprise เราไม่ได้มาโดยง่าย แล้ว Enterprise System คืออะไร ไม่ได้หมายถึงความยิ่งใหญ่อลังการมูลค่าสูง 
                            แต่หมายถึง "ความครอบคลุมทั่วถึงในระบบ" ตั้งแต่การควบคุมขั้นปฏิบัติการ(TIS) การมองภาพรวมเชิงการบริหาร(MIS)
                            ระบบสนับสนุนการตัดสินใจ(DSS) ไปจนถึงความรู้ที่ได้จากการสั่งสมประสบการณ์เพื่อเสนอแนวทางในการแก้ปัญหา(KMS)
                            </p>
                            
						</div>
						<div class="col-md-7">
							<div class="progress-bars">
								<div class="progress-label">
									<span>ลูกค้าธุรกิจ</span>
								</div>
								<div class="progress">
									<div class="progress-bar progress-bar-primary" data-appear-progress-animation="100%">
										<span class="progress-bar-tooltip">36</span>
									</div>
								</div>
								<div class="progress-label">
									<span>หน่วยงานภาครัฐ</span>
								</div>
								<div class="progress">
									<div class="progress-bar progress-bar-primary" data-appear-progress-animation="22%" data-appear-animation-delay="300">
										<span class="progress-bar-tooltip">8</span>
									</div>
								</div>
								<div class="progress-label">
									<span>รัฐวิสาหกิจ</span>
								</div>
								<div class="progress">
									<div class="progress-bar progress-bar-primary" data-appear-progress-animation="19%" data-appear-animation-delay="600">
										<span class="progress-bar-tooltip">7</span>
									</div>
								</div>
								<div class="progress-label">
									<span>ต่างประเทศ</span>
								</div>
								<div class="progress">
									<div class="progress-bar progress-bar-primary" data-appear-progress-animation="17%" data-appear-animation-delay="900">
										<span class="progress-bar-tooltip">6</span>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <h4 class="center">
                            เราขอขอบคุณลูกค้าทุกรายที่มอบความไว้วางใจให้เรา เป็นที่ปรึกษาและพัฒนาระบบธุรกิจอย่างต่อเนื่อง
                            </h4>
					<hr class="tall" />
				</div>

				<div class="container">
					<div class="row" id="team">
						<div class="col-md-12">

							<div class="row">

								<ul class="team-list">
									<li class="col-md-3 col-sm-6 col-xs-12">
										<div class="team-item thumbnail">
											<a href="contact.aspx" class="thumb-info team">
												<img alt="" height="270" src="img/team/Sathit.png" />
												<span class="thumb-info-title">
													<span class="thumb-info-inner">คุณสาธิต</span>
													<span class="thumb-info-type">R&D Manager</span>
												</span>
											</a>
											<span class="thumb-info-caption">
												<p>
                                                    การพัฒนาซอฟท์แวร์<br />
                                                    Enterprise Monitoring Tools<br />
                                                    การพัฒนาระบบ GIS<br />
                                                    ซอฟท์แวร์ระบบ GIS<br />                                 
                                                </p>
												<span class="thumb-info-social-icons">
													<a data-tooltip data-placement="bottom" target="_blank" href="http://www.facebook.com" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
													<a data-tooltip data-placement="bottom" href="http://www.twitter.com" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
													<a data-tooltip data-placement="bottom" href="http://www.linkedin.com" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
												</span>
											</span>
										</div>
									</li>
									
									<li class="col-md-3 col-sm-6 col-xs-12">
										<div class="team-item thumbnail">
											<a href="contact.aspx" class="thumb-info team">
												<img alt="" height="270" src="img/team/Chongrak.png" />
												<span class="thumb-info-title">
													<span class="thumb-info-inner">คุณจงรัก</span>
													<span class="thumb-info-type">GIS Specialist</span>
												</span>
											</a>
											<span class="thumb-info-caption">
												<p>
                                                    การพัฒนาระบบ GIS<br />
                                                    ซอฟท์แวร์ระบบ GIS<br /><br /><br />                   
                                                </p>
												<span class="thumb-info-social-icons">
													<a data-tooltip data-placement="bottom" target="_blank" href="http://www.facebook.com" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
													<a data-tooltip data-placement="bottom" href="http://www.twitter.com" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
													<a data-tooltip data-placement="bottom" href="http://www.linkedin.com" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
												</span>
											</span>
										</div>
									</li>
									<li class="col-md-3 col-sm-6 col-xs-12">
										<div class="team-item thumbnail">
											<a href="contact.aspx" class="thumb-info team">
												<img alt="" height="270" src="img/team/Teewin.png" />
												<span class="thumb-info-title">
													<span class="thumb-info-inner">ดร.ธีวินท์</span>
													<span class="thumb-info-type">Logistic Consultant</span>
												</span>
											</a>
											<span class="thumb-info-caption">
												<p>
                                                    ให้บริการที่ปรึกษาโลจิสจิกส์<br /><br /><br /><br />
                                                </p>
												<span class="thumb-info-social-icons">
													<a data-tooltip data-placement="bottom" target="_blank" href="http://www.facebook.com" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
													<a data-tooltip data-placement="bottom" href="http://www.twitter.com" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
													<a data-tooltip data-placement="bottom" href="http://www.linkedin.com" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
												</span>
											</span>
										</div>
									</li>
                                    <li class="col-md-3 col-sm-6 col-xs-12">
										<div class="team-item thumbnail">
											<a href="contact.aspx" class="thumb-info team">
												<img alt="" height="270" src="img/team/Tananat.png" />
												<span class="thumb-info-title">
													<span class="thumb-info-inner">คุณธนณัฏ</span>
													<span class="thumb-info-type">International Sales</span>
												</span>
											</a>
											<span class="thumb-info-caption">
												<p>
                                                    บริการแยกส่วนเครื่องจักร<br /><br /><br /><br />
                                                </p>
												<span class="thumb-info-social-icons">
													<a data-tooltip data-placement="bottom" target="_blank" href="http://www.facebook.com" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
													<a data-tooltip data-placement="bottom" href="http://www.twitter.com" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
													<a data-tooltip data-placement="bottom" href="http://www.linkedin.com" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
												</span>
											</span>
										</div>
									</li>
								</ul>

							</div>
						</div>
					</div>
				</div>

				<section class="call-to-action featured footer">
					<div class="container">
						<div class="row">
							<div class="center">
								<h3>หากคุณต้องการ เครื่องมือเพื่อพัฒนา <strong>ขีดความสามารถทางธุรกิจ</strong> <a href="contact.aspx" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">ปรึกษาเรา ฟรี!</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>
							</div>
						</div>
					</div>
				</section>

			</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">
</asp:Content>

