﻿Imports System.Net.Mail
Imports System.Net
Imports System.Data
Imports System.Data.OleDb

Partial Class contact
    Inherits System.Web.UI.Page

    Dim GL As New GenericLib
    Dim BL As New TIT_BL

    Protected Sub btnSend_Click(sender As Object, e As System.EventArgs) Handles btnSend.Click

        'Validate
        Dim TH_Msg As String = ""
        Dim EN_Msg As String = ""
        Dim Script As String = ""

        If txtName.Text = "" Then
            TH_Msg &= "ชื่อ, "
            EN_Msg &= "name, "
        End If
        If txtFrom.Text = "" Then
            TH_Msg &= "อีเมล, "
            EN_Msg &= "email, "
        ElseIf Not GL.IsValidEmailFormat(txtFrom.Text) Then
            TH_Msg &= "รูปแบบอีเมล, "
            EN_Msg &= "email format, "
        End If
        If txtSubject.Text = "" Then
            TH_Msg &= "หัวเรื่อง, "
            EN_Msg &= "subject, "
        End If
        If txtMessage.Text = "" Then
            TH_Msg &= "ข้อความ, "
            EN_Msg &= "message, "
        End If
        If TH_Msg <> "" Then
            contactError.Attributes("class") = "alert alert-danger"
            lblError.Text = "ไม่สามารถส่งเมลได้ กรุณาตรวจสอบ " & TH_Msg.Substring(0, TH_Msg.Length - 2) & "<br>Unable to send email, try to check your " & EN_Msg.Substring(0, EN_Msg.Length - 2)
            Script = "setTimeout(function(){ $('#" & contactError.ClientID & "').fadeOut(""slow"");}, 7000);"
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "FadeOut", Script, True)
            Exit Sub
        End If

        '-------------- Keep Cookie---------------
        Try
            Response.Cookies("UserContact_Name").Value = txtName.Text
            Response.Cookies("UserContact_Email").Value = txtFrom.Text
            Response.Cookies("UserContact_Phone").Value = txtPhone.Text
        Catch : End Try

        '---------------- Send Email-------------
        Try
            Dim smtp As SmtpClient = New SmtpClient()
            Dim message As New MailMessage()
            message.From = New MailAddress(txtFrom.Text)
            message.To.Add("info@tit-tech.co.th")
            message.Subject = txtSubject.Text
            message.IsBodyHtml = False
            message.Body = txtMessage.Text & vbNewLine & vbNewLine & "From : " & txtName.Text
            If txtPhone.Text <> "" Then
                message.Body &= vbNewLine & "Phone : " & txtPhone.Text
            End If
            smtp.Host = "mail.tit-tech.co.th"
            smtp.Credentials = New System.Net.NetworkCredential("info@tit-tech.co.th", "lkTb9cmidl6-")
            smtp.Send(message)
        Catch ex As Exception
            contactError.Attributes("class") = "alert alert-danger"
            lblError.Text = "ไม่สามารถส่งเมลได้ : " & ex.Message
            'Script = "setTimeout(function(){ $('#" & contactError.ClientID & "').fadeOut(""slow"");}, 10000);"
            'ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "FadeOut", Script, True)
            Exit Sub
        End Try
       

        '------- Clear Message-----------
        txtSubject.Text = ""
        txtMessage.Text = ""

        '------------ Report -------------
        contactSuccess.Attributes("class") = "alert alert-success"
        Script = "setTimeout(function(){ $('#" & contactSuccess.ClientID & "').fadeOut(""slow"");}, 5000);"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "FadeOut", Script, True)
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            btnSend.Attributes("onclick") &= "this.value='...Please Wait...'; "
            btnSend.Attributes("onclick") &= "document.getElementById('" & contactSuccess.ClientID & "').style.display='none';"
            btnSend.Attributes("onclick") &= "document.getElementById('" & contactError.ClientID & "').style.display='none';"

            '-------------- Set Default From Cookie---------------
            If Not IsNothing(Request.Cookies("UserContact_Name")) AndAlso Request.Cookies("UserContact_Name").Value <> "" Then
                txtName.Text = Request.Cookies("UserContact_Name").ToString
            End If
            If Not IsNothing(Request.Cookies("UserContact_Email")) AndAlso Request.Cookies("UserContact_Email").Value <> "" Then
                txtFrom.Text = Request.Cookies("UserContact_Email").Value
            End If
            If Not IsNothing(Request.Cookies("UserContact_Phone")) AndAlso Request.Cookies("UserContact_Phone").Value <> "" Then
                txtPhone.Text = Request.Cookies("UserContact_Phone").Value
            End If

            BuildKeyword()
        End If

        contactSuccess.Attributes("class") = "alert alert-success hidden"
        contactError.Attributes("class") = "alert alert-danger hidden"

    End Sub

    Private Sub BuildKeyword()

        Dim Keyword As String = "ติดต่อเรา, "
        Dim SQL As String = "SELECT * FROM [Sheet1$]"
        Dim DT As New DataTable
        Dim DA As New OleDbDataAdapter(SQL, BL.BuildExcelConnectionString(Server.MapPath("Keyword/Keyword.xls"), False))
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Keyword &= DT.Rows(i)(0).ToString & ", "
        Next
        If Keyword <> "" Then Keyword = Keyword.Substring(0, Keyword.Length - 2)
        metaKeyword.Attributes("content") = Keyword

    End Sub
End Class
