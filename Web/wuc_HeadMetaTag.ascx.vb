﻿Imports System.Data
Imports System.Data.OleDb

Partial Class wuc_HeadMetaTag
    Inherits System.Web.UI.UserControl

    Dim BL As New TIT_BL

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim Desc As String = "บริษัท ไทย อิเมจิเนชั่นเทคโนโลยี จำกัด บริษัทที่ปรึกษาพัฒนาซอฟท์แวร์ วิเคราะห์ระบบธุรกิจเพื่อออกแบบระบบฐานข้อมูล พัฒนาระบบเทคโนโลยีสานสนเทศและสารสนเทศภูมิศาสตร์ GIS เพื่อใช้ในธุรกิจ บริการที่ปรึกษาพัฒนาระบบการจัดการโลจิสติกส์สำหรับองค์กร บริการแยกส่วนเครื่องบินเหมาลำเพื่อส่งออกชิ้นส่วนอลูมิเนียมและไททาเนียมเพื่อนำไปผลิตเป็นวัตถุดิบในอุตสาหกรรมใหม่"
            Dim Keyword As String = ""

            Dim ConnStr As String = BL.BuildExcelConnectionString(Server.MapPath("Keyword/Keyword.xls"), False)

            Dim SQL As String = "SELECT * FROM [Sheet1$]"
            Dim DT As New DataTable
            Dim DA As New OleDbDataAdapter(SQL, ConnStr)
            DA.Fill(DT)
            For i As Integer = 0 To DT.Rows.Count - 1
                If DT.Rows(i).Item(0).ToString = "" Then Exit For
                Keyword &= DT.Rows(i)(0).ToString & ", "
            Next
            If Keyword <> "" Then Keyword = Keyword.Substring(0, Keyword.Length - 2)
            metaKeyword.Attributes("content") = Keyword
            metaDescription.Attributes("content") = Desc
        End If

    End Sub
End Class
