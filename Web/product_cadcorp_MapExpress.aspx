﻿<%@ Page Title="CadcorpSIS Map Express (Free GIS Software)" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="product_cadcorp_MapExpress.aspx.vb" Inherits="product_cadcorp_MapExpress" %>

<%@ Register src="wuc_CadcorpMetaTag.ascx" tagname="wuc_HeadMetaTag" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<uc1:wuc_HeadMetaTag ID="wuc_HeadMetaTag1" runat="server" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

    <style type="text/css">
            .page-top
            {
                margin-bottom:0px;
                }
            .custom-product
            {               
                margin-bottom:35px;
                }
                
            .page-top.custom-product
            {
                 background-image: url(img/cadcorp/bg_blue_1920x700.jpg);
                }
            
    </style>

     <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">									
						<li class="active">CadcorpSIS</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Map Express (Free GIS Software)</h1>
				</div>
			</div>
		</div>
	</section>

    <section class="page-top custom-product">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<h1>ฟรีแวร์ Desktop GIS จาก <strong> CadcorpSIS</strong>.</h1>
					<p class="lead">เปิดไฟล์ข้อมูลแผนที่ได้ทุกนามสกุล!!
อีกทั้งยังสามารถเปิดไฟล์แผนที่จาก GIS Software, CAD, Web Map Service และ Database </p>
					<a href="http://www.cadcorp.com/products/free-mapping-software" target="_blank" class="btn btn-default btn-lg push-bottom">ดาวน์โหลดฟรี</a> 
                </div>
				<div class="col-sm-5">
					<img class="pull-right responsive" alt="Free GIS Software" src="img/cadcorp/MapExpress_Landing.png" />
				</div>
			</div>
		</div>
	</section>
    <div class="container">
        <div class="row">
		    <div class="col-sm-7">
			    <h2><strong>คุณสมบัติพิเศษ</strong> ของ Map Express </h2>
			    <p class="lead">
				    Free GIS Software ระดับเริ่มต้น ผู้ใช้สามารถทดลองคุณสมบัติพื้นฐาน Desktop GIS ของ CadcorpSIS ซึ่ง Map Express สามารถอ่านไฟล์ข้อมูล GIS ได้เกือบทุกนามสกุล
                    รวมถึงการเปิดไฟล์แผนที่ตามมาตรฐาน OGC (Open GIS Consortium) เช่น Web Map Server, Web Map Feature, Database, CAD เป็นต้น 
               </p>                
		    </div>

		    <div class="col-sm-4 col-sm-offset-1 push-top">
			    <img class="img-responsive" src="img/cadcorp/cadcorp_free_200x200.png">
		    </div>
	    </div>

        <hr class="tall" />

        <div class="row">			
				<div class="col-md-6">
						<div class="feature-box secundary">
							<div class="feature-box-icon">
								<i class="fa fa-search"></i>
							</div>
							<div class="feature-box-info">
								<h4 class="shorter">spatial querying</h4>								
							</div>
						</div>
						<div class="feature-box secundary">
							<div class="feature-box-icon">
								<i class="fa fa-search"></i>
							</div>
							<div class="feature-box-info">
								<h4 class="shorter">attribute querying</h4>								
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="feature-box secundary">
							<div class="feature-box-icon">
								<i class="fa fa-table"></i>
							</div>
							<div class="feature-box-info">
								<h4 class="shorter">tabular visualisation</h4>								
							</div>
						</div>
						<div class="feature-box secundary">
							<div class="feature-box-icon">
								<i class="fa fa-bar-chart"></i>
							</div>
							<div class="feature-box-info">
								<h4 class="shorter">statistical analysis</h4>								
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="feature-box secundary">
							<div class="feature-box-icon">
								<i class="fa fa-pie-chart"></i>
							</div>
							<div class="feature-box-info">
								<h4 class="shorter">advanced thematic mapping</h4>								
							</div>
						</div>
						<div class="feature-box secundary">
							<div class="feature-box-icon">
								<i class="fa fa-cube"></i>
							</div>
							<div class="feature-box-info">
								<h4 class="shorter">3D visualisation</h4>								
							</div>
						</div>
					</div>
                    <div class="col-md-6">
						<div class="feature-box secundary">
							<div class="feature-box-icon">
								<i class="fa fa-file-pdf-o"></i>
							</div>
							<div class="feature-box-info">
								<h4 class="shorter">map publishing in PDF</h4>								
							</div>
						</div>
						<div class="feature-box secundary">
							<div class="feature-box-icon">
								<i class="fa fa-print"></i>
							</div>
							<div class="feature-box-info">
								<h4 class="shorter">map printing</h4>								
							</div>
						</div>
					</div>				
		</div>

        <hr class="tall" />

       <div class="row">
		    <div class="col-sm-6">
			    <h2><strong>ระบบปฏิบัติการ</strong> ที่รองรับ </h2>
			    
				    <div class="row">
			            <div class="col-md-12">				    				            
						        <div class="feature-box secundary">
							        <div class="feature-box-icon">
								        <i class="fa fa-windows"></i>
							        </div>
							        <div class="feature-box-info">
								        <h4 class="shorter">Microsoft Windows Vista</h4>								
							        </div>
						        </div>
						        <div class="feature-box secundary">
							        <div class="feature-box-icon">
								        <i class="fa fa-windows"></i>
							        </div>
							        <div class="feature-box-info">
								        <h4 class="shorter">Microsoft Windows 7</h4>								
							        </div>
						        </div>					           
						        <div class="feature-box secundary">
							        <div class="feature-box-icon">
								        <i class="fa fa-windows"></i>
							        </div>
							        <div class="feature-box-info">
								        <h4 class="shorter">Microsoft Windows 8</h4>								
							        </div>
						        </div>
                                <div class="feature-box secundary">
							        <div class="feature-box-icon">
								        <i class="fa fa-ban"></i>
							        </div>
							        <div class="feature-box-info">
								        <h4 class="shorter">ไม่รองรับ Microsoft Windows XP</h4>								
							        </div>
						        </div>			
			            </div>
		            </div>               
		    </div>
            <div class="col-sm-6">
                   <h2><strong>VDO </strong> สาธิตการใช้งาน</h2>
                   <div class="row">
                          <div class="col-md-12"> 
                                <iframe class="product_vdo" src="https://www.youtube.com/embed/146HZ-cWlF8" frameborder="0" allowfullscreen></iframe>
                            </div>
                   </div>
            </div>
		    
	    </div>

       

    </div>
    
     <section class="call-to-action featured footer">
		<div class="container">
			<div class="row">
				<div class="center">
					<h3>คุณสามารถ <strong>ดาวน์โหลดน์ CadcorpSIS Map Express เพื่อใช้งานได้ฟรี!</strong> <a href="http://www.cadcorp.com/products/free-mapping-software" target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">คลิ้กดาวน์โหลด</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>
				</div>
			</div>
		</div>
	</section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">
</asp:Content>

