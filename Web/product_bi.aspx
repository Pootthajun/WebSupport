﻿<%@ Page Title="Business Intelligence(BI) and Dashboard" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="product_bi.aspx.vb" Inherits="product_bi" %>

<%@ Register src="~/wuc_ERPBIMetaTag.ascx" tagname="wuc_ERPBIMetaTag" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<uc1:wuc_ERPBIMetaTag ID="wuc_ERPBIMetaTag" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">									
						<li class="active">Enterprise Monitoring Tools</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Business Intelligence(BI) and Dashboard</h1>
				</div>
			</div>
		</div>
	</section>
    <div class="container">
        <h2><strong>Business Intelligence (BI)</strong> คืออะไร</h2>
		<div class="row">
			<div class="col-md-12">
				<p class="lead">
					Business Intelligence หรือ BI คือ เทคโนโลยีที่ช่วยในการสรุปภาพรวมของข้อมูลทางธุรกิจในหลายมิติ เพื่อสะท้อนผลการดำเนินงาน
                    และสนับสนุนทางเลือกในการตัดสินใจและช่วยในการวางแผนทางธุรกิจ
				</p>                
			</div>            
        </div>
        <div class="row">
            <div class="col-md-4">
				 <p><img class="img-responsive" src="img/products/BI1.png" alt="ตัวอย่าง BI" /></p>  
			</div>    
            <div class="col-md-4">  
                 <p><img class="img-responsive" src="img/products/BI2.png" alt="ตัวอย่าง BI" /></p>
			</div> 
            <div class="col-md-4">    
                 <p><img class="img-responsive" src="img/products/BI3.png" alt="ตัวอย่าง BI" /></p>
			</div> 
        </div>
        <hr>
        <h2><strong>Summary Statistic Analysis</strong> ได้มาอย่างไร</h2>
        <div class="row">
			<div class="col-md-6">
				<p class="lead">
					ในการสร้าง Dashboard หรือ BI นั้นวัตถุดิบที่สำคัญคือ"ข้อมูล" ในระหว่างที่เราดำเนินธุรกิจหากเราใช้เทคโนโลยีสารสนเทศและซอฟท์แวร์การจัดการฐานข้อมูลมาช่วยในการทำงาน
                    ไม่ว่าจะเป็น Custom ERP หรือระบบ ERP สำเร็จรูป ข้อมูลจะถูกรวบรวมสั่งสมตลอดเวลา ยิ่งนานวันเข้าข้อมูลที่เราสะสมอยู่ในระบบเป็นจำนวนมหาศาล
                    โดยในหลักการของ Software Engineering เรียกข้อมูลสะสมเหล่านี้ว่า Archive Transaction คือประวัติและสถิติทั้งหมดของธุรกิจนั้นๆ </p>                
			</div>		
            <div class="col-md-6">
				<img class="img-responsive" src="img/products/ETL.png" alt="กระบวนการตกผลึกข้อมูล" />                   
			</div>					
        </div>
        <div class="row">
            <div class="col-md-4">
                <img class="img-responsive" src="img/products/BI_Data_Concept1.png" alt="ข้อมูลในระบบธุรกิจ" />                
            </div>
            <div class="col-md-8">
                <p class="lead">
                    ดังนั้นเราสามารถนำข้อมูลเหล่านี้มาตกผลึกและกลั่นกรอง เพื่อทำรายงานหรือแผนภูมิสรุปภาพรวมทางธุรกิจในมุมหรือมิติที่เราสนใจได้ ด้วยขั้นตอนทางวิศวกรรมซอฟท์แวร์ ซึ่งไม่ขอลงรายละเอียด(เดี๋ยวยาว)
                    โดยข้อมูลที่เราสรุปภาพรวมมิติต่างๆนั้น เรามานำมาใช้เพื่อแสดงรายงานสรุปภาพรวม แสดง Trend หรือแนวโน้มในด้านต่างๆ ใช้ในการวิเคราะห์หาจุดที่ต้องแก้ไขปรับปรุง
                    รวมถึงการพยากรณ์คาดการณ์เหตุการล่วงหน้าที่กำลังจะเกิดขึ้น เพื่อเป็นข้อมูลประกอบการตัดสินใจให้กับผู้บริหาร
                </p>                   
            </div>
            
        </div>
        <div class="row"> 
            <div class="col-md-12">
                <p class="tall">
					จากแผนภาพ เราจะเห็นความสัมพันธ์ระหว่างปริมาณข้อมูลในธุรกิจกับบทบาทของบุคคลากร
                    ดังนั้นการจะทำบทวิเคราะห์ในมิติต่างๆนั้น จำเป็นจะต้องทำการสรุปภาพรวมข้อมูล เพื่อให้ BI Software ใช้ประโยชน์จากข้อมูลที่สรุปแล้วได้โดยตรง                                            
				</p> 
            </div>            
        </div>
        <hr>
        <h2>หน้าที่ของ <strong>Business Intelligence (BI) Software</strong></h2>
        <div class="row">
			<div class="col-md-8">
				<p class="lead">
					Business Intelligence หรือ BI คือเทคโนโลยีหรือซฟอท์แวร์ที่ช่วยนำข้อมูลที่ผ่านการตกผลึกและวิเคราะห์ในมิติต่างๆ มาแสดงผลในรูปของรายงาน
                    กราฟ ตารางสรุป และบทวิเคราะห์ เพื่อให้กลุ่มผู้บริหารหรือผู้มีอำนาจตัดสินใจเข้าใจง่าย โดยทั่วไปมักอยู่ในรูปซอฟท์แวร์สำเร็จรูป
				</p>
			</div>	
            <div class="col-md-4">
				 <p><img class="img-responsive" src="img/products/bi-tools.jpg" alt="ตัวอย่าง BI" /></p>  
			</div>  					
        </div>
        <div class="row">
           <div class="col-md-12">
               <p class="lead">
                    ซึ่งในมุมของเรา เรามองว่ากระบวนการคิดของซอฟท์แวร์ BI ไม่ได้มีความซับซ้อนแต่อย่างใดเนื่องจากเป็นการนำข้อมูลที่ผ่านวิเคราะห์ในมิติต่างๆมาแสดงผล 
                    แต่ขั้นตอนที่สำคัญและภาระหนักของการทำระบบ Business Intelligence คือการเตรียมข้อมูล การวิเคราะห์และตกผลึกข้อมูลมากกว่า 
                </p>
           </div>  	    
        </div>
        <hr>
        <% 'ความแตกต่างระหว่าง BI และ Dashboard %>>
        <h2><strong>บริการของเรา</strong></h2>
        <div class="row">
           <div class="col-md-12">
               <p class="lead">
                    เราให้บริการปรึกษาสำหรับองค์กรธุรกิจ ที่ต้องการพัฒนาระบบ Business Intelligence และ Dashboard ทั้งรูปแบบของการนำเสนอซอฟท์แวร์สำหรับรูป
                    และการพัฒนาระบบงานขึ้นมาเฉพาะสำหรับหน่วยงานของท่าน ซึ่งเรามีทีมงานที่มีความเชี่ยวชาญในการวิเคราะห์ข้อมูลร่วมกับหลักทฤษฎีทางวิชาการ ในทุกมิติที่อาจคาดไม่ถึง
                    เพื่อช่วยในการสรุปภาพรวมเชิงธุรกิจ วางแผนแนวโน้มการจัดการเพื่อรับความเสี่ยง การปรับปรุงประสิทธิภาพการทำงานในองค์กร 
                    ช่วยให้ท่านประสบความสำเร็จในการวางแผนกลยุทธสำหรับธุรกิจ ทั้งเชิงรุกและเชิงรับได้อย่างมั่นใจ
                </p>
           </div>  	    
        </div>
    </div>
            
    <section class="call-to-action featured footer">
		<div class="container">
			<div class="row">
				<div class="center">
					<h3>หากท่านต้องการข้อมูลเพิ่มเติม <a href="contact.aspx" target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">ติดต่อเรา</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span>
                    เรายินดีให้บริการ
                    </h3>
				</div>
			</div>
		</div>
	</section>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">
</asp:Content>

