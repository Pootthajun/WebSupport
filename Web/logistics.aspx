﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="logistics.aspx.vb" Inherits="logistics" %>

<%@ Register src="~/wuc_ERPBIMetaTag.ascx" tagname="wuc_ERPBIMetaTag" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<uc1:wuc_ERPBIMetaTag ID="wuc_ERPBIMetaTag" runat="server" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">									
						<li class="active">Logistics Consulting</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>บริการที่ปรึกษาการพัฒนา Logistics</h1>
				</div>
			</div>
		</div>
	</section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">
</asp:Content>

