﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="about_company.aspx.vb" Inherits="about_company" %>

<%@ Register src="wuc_HeadMetaTag.ascx" tagname="wuc_HeadMetaTag" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
<uc1:wuc_HeadMetaTag ID="wuc_HeadMetaTag1" runat="server" />
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

                <section class="page-top">
					<div class="container">	
                        <div class="row">
				            <div class="col-md-12">
					            <ul class="breadcrumb">									
						            <li class="active">เกี่ยวกับเรา</li>
					            </ul>
				            </div>
			            </div>					
						<div class="row">
							<div class="col-md-12">
								<h1>ประวัติบริษัท</h1>
							</div>
						</div>
					</div>
				</section>

                <div class="container">

					<h2 class="word-rotator-title">
						เส้นทางใหม่สู่ <strong>
							<span class="word-rotate" data-plugin-options='{"delay": 2000}'>
								<span class="word-rotate-items">
									<span>ความสำเร็จ</span>
									<span>ความเป็นคุณ</span>
									<span>การสร้างสรรค์</span>
                                    <span>การพัฒนา</span>
								</span>
							</span>
						</strong>
					</h2>

					<div class="row">
						<div class="col-md-10">
							<p class="lead">
								บริษัท ไทยอิเมจิเนชั่นเทคโนโลยี จำกัด เรามีเป้าหมายทางธุรกิจเพื่อวิเคราะห์โครงสร้างการดำเนินธุรกิจและ
                                นำเทคโนโลยีที่มีอยู่ในปัจจุบัน ทั้งด้านซอฟท์แวร์ ระบบจัดการสารสนเทศ ระบบสื่อสารโทรคมนาคม เทคโนโลยีด้านการจัดการโลจิสติกส์ เทคโนโลยีการเกษตรและการประมง
                                มาช่วยเป็นเครืองมือในการปรับปรุงเพิ่มขีดความสามารถทางธุรกิจให้กับลูกค้า 
                                <br /><br />
                                โดยเป้าหมายสูงสุดขององค์กร เพื่อวางรากฐานการสร้างแนวคิดให้บุคคลากร ต่อยอดไปสู่การสร้างสรรค์นวัตกรรม
                                และเป็นผู้นำด้านการวิจัยนวัตกรรมและพัฒนาเทคโนโลยีที่เหมาะสมสำหรับภูมิภาค รวมถึงการสร้างความมั่นคงด้านพลังงานและสาธารณูปโภคให้แก่ประเทศในอนาคต
							</p>
						</div>
						<div class="col-md-2">
							<a href="#" class="btn btn-lg btn-primary push-top">ร่วมงานกับเรา!</a>
						</div>
					</div>

					<hr class="tall">

					<div class="row">
						<div class="col-md-8">
							<h3><strong>เรา</strong> คือใคร</h3>
							<p>
                                มีคำถามว่าคนไทยจะพัฒนานวัตกรรมได้หรือ ซึ่งหากเราได้ยินคำถามนี้เราจะไม่แปลกใจ เพราะผู้ถามอาจยังไม่ทราบว่า
                                เทคโนโลยีที่สมัยใหม่ที่เราเข้าใจว่าชาวต่างชาติเป็นผู้คิดค้น แต่หารู้ไม่ว่าเบื้องหลังมีนักวิจัยชาวไทยเข้าไปมีส่วนร่วมไม่น้อย 
                                เพราะเราเคยทำมาแล้ว!! หากเพียงแต่ติดอยู่ที่เงื่อนไขบางอย่าง รวมถึงเรื่องลิขสิทธิ์เป็นข้อผูกมัด                                 
                            </p>
							<p>
                                บริษัท ไทยอิเมจิเนชั่นเทคโนโลยี จำกัด จัดตั้งขึ้นโดยกลุ่มทีมงานที่มีความเชี่ยวชาญในหลากสาขาอาชีพ ได้แก่ วิศกรหลากสาขา ผู้เชี่ยวชาญด้านโลจิสติกส์ นักวิเคราะห์ระบบ 
                                นักสถาปัตกรรมเครือข่ายโทรคมนาคม นักวิจัย รวมตัวกันเพื่อก่อตั้งเป็นกลุ่มองค์กรที่มีเป้าหมายเดียวกัน ทำในสิ่งที่มีอุดมการร่วมกัน เพื่อสร้างสรรค์แลกเปลี่ยนแบ่งปันแนวคิดใหม่ๆ 
                                ในสาขาที่ตนถนัด ทำให้เราสามารถตอบโจทย์ความต้องการที่หลากหลายของลูกค้าได้ตรงเป้าหมาย
                            </p>
                            <p>
                                เป้าหมายสำคัญที่สุดของเรา มิใช่เพื่อหวังแค่ผลกำไรทางธุรกิจอันมักง่ายฉาบฉวย แต่หลักสำคัญคือ "เพื่อพัฒนาความยั่งยืนในแนวความคิด" ให้แก่ทั้งลูกค้า พันธมิตรผู้ร่วมงาน
                                และภายในทีมงานของเราเอง โดยการปลูกฝังแนวความคิดเชิงนวัตกรรมนั้น เราถือว่าเป็นผลกำไรที่มีมูลค่ามหาศาลที่ทั้งทีมงานและลูกค้าจะได้รับ 
                                แม้ว่าสิ่งนี้อาจเป็นสิ่งเล็กๆที่ ณ วันนี้อาจจะยังจับต้องไม่ได้ แต่นี่คือสิ่งสำคัญที่ทำให้เราพัฒนาต่อไปร่วมกันอย่างยั่งยืน
                            </p>
						</div>
						<div class="col-md-4">
							<div class="featured-box featured-box-secundary">
								<div class="box-content">
									<h4>ภาพกิจกรรมบางส่วน</h4>
									<ul class="thumbnail-gallery" data-plugin-options='{"delegate": "a", "type": "image", "gallery": {"enabled": true}}' data-plugin-lightbox="">
										
										<li>
											<a title="ผลงานระบบรายงานการนำเข้าผลิตภัณฑ์มันสำปะหลัง กระทรวงพาณิชย์" href="img/behindscene/2.jpg"">
												<span class="thumbnail">
													<img src="img/behindscene/2.jpg" width="75">
												</span>
											</a>
										</li>
										<li>
											<a title="ประชุมที่กระทรวงพาณิชย์" href="img/behindscene/3.jpg"">
												<span class="thumbnail">
													<img src="img/behindscene/3.jpg" width="75">
												</span>
											</a>
										</li>
										<li>
											<a title="นำเสนอระบบทรัพยากรบุคคลโรงงานยาสูบ" href="img/behindscene/4.jpg"">
												<span class="thumbnail">
													<img src="img/behindscene/4.jpg" width="75">
												</span>
											</a>
										</li>
										<li>
											<a title="นำเสนอแนวคิดการประยุกต์ใช้ระบบสารสนเทศภูมิศาสตร์ GIS ที่สถาบันพัฒนาองค์กรชุมชนแห่งชาติ(องค์การมหาชน)" href="img/behindscene/5.jpg"">
												<span class="thumbnail">
													<img src="img/behindscene/5.jpg" width="75">
												</span>
											</a>
										</li>
                                        <li>
											<a title="วิเคราะห์สถิติเชิงพื้นที่ สถาบันพัฒนาองค์กรชุมชน(องค์การมหาชน)" href="img/behindscene/6.jpg"">
												<span class="thumbnail">
													<img src="img/behindscene/6.jpg" width="75">
												</span>
											</a>
										</li>
										<li>
											<a title="ชิ้นส่วนเครื่อง Boing 747 ที่เตรียมตัดแยก" href="img/behindscene/7.jpg"">
												<span class="thumbnail">
													<img src="img/behindscene/7.jpg" width="75">
												</span>
											</a>
										</li>
                                        <li>
											<a title="เข้าร่วมประชาพิจารณ์แผนแม่บท สถาบันเทคโนโลยีวิจัยเพื่อการเกษตร กรมพัฒนาที่ดิน" href="img/behindscene/11.jpg"">
												<span class="thumbnail">
													<img src="img/behindscene/11.jpg" width="75">
												</span>
											</a>
										</li>
                                        <li>
											<a title="ชิ้นส่วนเครื่อง Boing 747 ที่เตรียมตัดแยก" href="img/behindscene/8.jpg"">
												<span class="thumbnail">
													<img src="img/behindscene/8.jpg" width="75">
												</span>
											</a>
										</li>
                                        <li>
											<a title="งานแยกชิ้นส่วนหาง Boing 747" href="img/behindscene/9.jpg"">
												<span class="thumbnail">
													<img src="img/behindscene/9.jpg" width="75">
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<hr class="tall">

					<div class="row">
						<div class="col-md-12">
							<h3 class="push-top"><strong>ภาพรวม</strong> ความก้าวหน้า</h3>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">

							<ul class="history">
								<li data-appear-animation="fadeInUp">
									<div class="thumb">
										<img src="img/year/4.png" alt="" />
									</div>
									<div class="featured-box">
										<div class="box-content">
											<h4><strong>2015</strong> เปิดช่องทางการขยายกลุ่มลูกค้าไปยังต่างประเทศเพื่อส่งออกและรองรับนวัตกรรมใหม่</h4>
										</div>
									</div>
								</li>
                                <li data-appear-animation="fadeInUp">
									<div class="thumb">
										<img src="img/year/3.png" alt="" />
									</div>
									<div class="featured-box">
										<div class="box-content">
											<h4><strong>2014</strong> มุ่งเน้นการพัฒนาระบบสารสนเทศภูมิศาสตร์ และพัฒนาบุคคลากรรุ่นใหม่</h4>
										</div>
									</div>
								</li>
								<li data-appear-animation="fadeInUp">
									<div class="thumb">
										<img src="img/year/2.png" alt="" />
									</div>
									<div class="featured-box">
										<div class="box-content">
											<h4><strong>2013</strong> เพิ่มการพัฒนาขีดความสามารถระบบทรัพยากรบุคคล</h4>
										</div>
									</div>
								</li>
								<li data-appear-animation="fadeInUp">
									<div class="thumb">
										<img src="img/year/1.png" alt="" />
									</div>
									<div class="featured-box">
										<div class="box-content">
											<h4><strong>2012</strong> ก่อตั้งบริษัท และพัฒนาระบบสารสนเทศสำหรับภาครัฐ รัฐวิสาหกิจ และเอกชน</h4>
											
										</div>
									</div>
								</li>
								
							</ul>

						</div>
					</div>

				</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">
</asp:Content>

