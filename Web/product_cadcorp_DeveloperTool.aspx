﻿<%@ Page Title="CadcorpSIS Developer Tools" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="product_cadcorp_DeveloperTool.aspx.vb" Inherits="product_cadcorp_DeveloperTool" %>

<%@ Register src="wuc_CadcorpMetaTag.ascx" tagname="wuc_HeadMetaTag" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<uc1:wuc_HeadMetaTag ID="wuc_HeadMetaTag1" runat="server" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

    <style type="text/css">
            .page-top
            {
                margin-bottom:0px;
                }
            .custom-product
            {               
                margin-bottom:35px;
                }
                
                      
    </style>

    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">									
						<li class="active">CadcorpSIS</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Developer Tools</h1>
				</div>
			</div>
		</div>
	</section>

    <section class="page-top custom-product">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<h1>เครื่องมือพัฒนาซอฟท์แวร์จาก<strong> CadcorpSIS</strong>.</h1>
					<p class="lead">  
                        โดยการใช้ประโยชน์จากระบบสารสนเทศภูมิศาสตร์ GIS อย่างเต็มประสิทธิภาพ 
                        คุณสามารถพัฒนาซอฟท์แวร์และแอพพลิเคชั่น GIS เพื่อใช้ในองค์กรของคุณเองได้ ไม่ว่าจะเป็นแอพพลิเคชั่นบนมือถือ Smart Phone 
                        แอพพลิเคชั่นบนเว็บไซต์ หรือแม้กระทั่ง แอพพลิเคชั่นบน Desktop ของคุณ ด้วยเครื่องมือพัฒนาซอฟท์แวร์ GIS จาก CadcorpSIS
                    </p>
				</div>
				<div class="col-sm-5">
					<img class="pull-right responsive" src="img/cadcorp/smartphone_composite_640x464.png" alt="GIS on Smart Mobile Device" />
				</div>
			</div>
		</div>
	</section>

    <div class="container">
        <div class="row">
		    <div class="col-sm-7">
			    <h2>เครื่องมือพัฒนา GIS แอพพลิเคชั่นจาก<strong>CadcorpSIS</strong></h2>
			    <p class="lead">
				    เครื่องมือพัฒนาซอฟท์แวร์สำหรับนักพัฒนาระบบ GIS รองรับการพัฒนาแอพพลิเคชั่นได้หลาย Platform ไม่ว่าจะเป็น Smart Mobile Device, Web Application 
                    หรือแม้แต่การสร้าง Application บน Desktop PC ก็สามารถทำได้โดยง่าย  ด้วยชุดเครื่องมือที่มี 
                    โดยคุณสามารถเลือกใช้เครื่องมือในการพัฒนาได้ตาม Environment ที่คุณถนัด

               </p>
		    </div>

		    <div class="col-sm-4 col-sm-offset-1 push-top">
			    <img class="img-responsive" src="img/cadcorp/desktop_200x200.png">
		    </div>
	    </div>
        <hr class="tall" />
        <div class="row custom-product"> 
		    <div class="col-sm-6">
			    <h2><strong>ActiveX SDK</strong> </h2>			    
				    <div class="row">
			            <div class="col-md-12">				    				            
						    เป็นเครื่องมือพัฒนซอฟท์แวร์ GIS บน Desktop PC ซึ่งมี Core Engine เดียวกันกับ Map Modeller 
                            เพื่อพัฒนาแอพพลิเคชั่น GIS ให้เหมาะสมกับความต้องการทางธุรกิจในองค์กรของคุณเอง โดยการพัฒนา ActiveX
                            SDK นั้น สามารถพัฒนาด้วยภาษาที่คุณถนัดไม่ว่าจะเป็น VB.NET, C# หรือ Visual C++                                                            	
			            </div>
		            </div>
                    <div class="col-sm-6 push-top">
                            <a href="http://www.cadcorp.com/products/developers-tools/activex-sdk" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-envelope"></i>เพิ่มเติม</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
                    </div>
		    </div>
            <div class="col-sm-6 push-top">
                    <img class="img-responsive" src="img/cadcorp/Microsoft-.NET_.jpg" alt="Microsoft.NET" />
            </div>
            
	    </div>

        <div class="row custom-product"> 
		    <div class="col-sm-6">
			    <h2><strong>Desktop Add-ins</strong> </h2>			    
				    <div class="row">
			            <div class="col-md-12">				    				            
						    เป็นการพัฒนา Plug-in เสริมในซอฟท์แวร์ Desktop GIS ของ CadcorpSIS คือ Map Manager, Map Editor และ Map Modeller 
                            เพื่อให้เจ้าหน้าที่ระบบสารสนเทศภูมิศาสตร์ทำงานได้ง่ายขึ้น โดยอาศัยการพัฒนาบน Core Engine ของ ActiveX SDK ด้วยภาษาที่คุณถนัดไม่ว่าจะเป็น VB.NET, C# หรือ Visual C++   
                                                                             	
			            </div>
		            </div>
                    <div class="col-sm-6 push-top">
                            <a href="http://www.cadcorp.com/products/developers-tools/desktop-add-ins" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-envelope"></i>เพิ่มเติม</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
                    </div>
		    </div>
            <div class="col-sm-6 push-top">
                    <img class="img-responsive" src="img/cadcorp/plugins.png" alt="Software Plugin" />
            </div>
            
	    </div>

        <div class="row custom-product"> 
		    <div class="col-sm-6">
			    <h2><strong>GeognoSIS Developer</strong> </h2>			    
				    <div class="row">
			            <div class="col-md-12">				    				            
						    เป็นเครื่องมือพัฒนาแอพพลิเคชั่นในรูปแบบของ Web Application และ Web Map Service  
                            ผู้พัฒนาสามารถพัฒนา GIS Application ด้วยทุกภาษาไม่ว่าจะเป็น Classic ASP, PHP, JSP และ Microsoft Studio.NET
                            โดยการเรียกใช้ข้อมูลแผนที่จาก Web Service ผ่านโปรโตคอล SOAP รวมไปถึง HTTP Web Request  	
			            </div>
		            </div>
                    <div class="col-sm-6 push-top">
                            <a href="http://www.cadcorp.com/products/developers-tools/geognosis-developer" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-envelope"></i>เพิ่มเติม</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
                    </div>
		    </div>
            <div class="col-sm-6 push-top">
                    <img class="img-responsive" src="img/cadcorp/GeognoSIS.png" alt="GeognoSIS Developer" />
            </div>
            
	    </div>

        <div class="row custom-product"> 
		    <div class="col-sm-6">
			    <h2><strong>Web Map Development</strong> </h2>			    
				    <div class="row">
			            <div class="col-md-12">	
                        เป็น Addin เสริมใน Microsoft Visual Studio.NET ซึ่งช่วยให้นักพัฒนาซอฟท์แวร์สามารถสร้าง Web Application 
                        ได้โดยง่าย โดยเรียกใช้ Control ผ่าน NameSpace : Web Map Web.UI ซึ่งรวมเอาฟังก์ชั่น JavaScript ที่ช่วยให้ผู้ใช้สามารถ
                        เรียกใช้งานข้อมูลแผนที่ GIS ได้อย่างรวดเร็วและง่ายดาย ช่วยลดขั้นตอนการพัฒนาซอฟท์แวร์ได้มาก โดยการทำงานของ Addin นี้
                        จะทำงานบนพื้นฐานของ Web Map Service และ Web Feature Servince ทำให้ระบบสามารถเรียกใช้ข้อมูล GIS ที่องค์กรมีอยู่
                        ร่วมกับข้อมูล GIS ผู้ให้บริการรายอื่นๆเผยแพร่บน Web Service ทำให้การพัฒนาสะดวกและมีประสิทธิภาพมากขึ้น 
                        รวมถึงการใช้ประโยชน์จากข้อมูล GIS ได้สูงสุด
                         <div class="col-sm-6 push-top">
                                <a href="http://www.cadcorp.com/products/web-mapping/" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-envelope"></i>เพิ่มเติม</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
                        </div>  

			            </div>
		            </div>
		    </div>
            <div class="col-sm-6 push-top">
                 <img class="img-responsive" src="img/cadcorp/WebMapDevelopment.png" alt="Web Map Development" />
            </div>
	    </div>
     </div>

     <section class="call-to-action featured footer">
		<div class="container">
			<div class="row">
				<div class="center">
					<h3> 
                    หากต้องการข้อมูลเพิ่มเติม  กรุณา
                    <a href="contact.aspx" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">สอบถามข้อมูลเพิ่มเติม</a> </strong>
                   เรายินดีให้คำปรึกษาโดยไม่มีค่าใช้จ่ายใดๆ
                    </h3>
				</div>
			</div>
		</div>
	</section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">
</asp:Content>

