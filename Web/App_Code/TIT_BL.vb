﻿Imports Microsoft.VisualBasic

Public Class TIT_BL

    Public ConnectionString As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Public Function BuildExcelConnectionString(ByVal Path As String, ByVal IsFirstRowHeader As Boolean) As String
        Dim ConnStr As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Path & "; Extended Properties=""Excel 8.0;"

        If IsFirstRowHeader Then
            ConnStr &= "HDR=Yes;IMEX=1"";"
        Else
            ConnStr &= "HDR=No;IMEX=1"";"
        End If
        Return ConnStr
    End Function
End Class
