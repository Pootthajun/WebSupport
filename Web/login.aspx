﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" runat="Server">
    <div class="featured-box featured-box-secundary default info-content" style="width:550px" id="content-box">
        <div class="box-content">       
            <h4 class="text-center" align="left">
                Login</h4>
            <form action="" id="" method="post">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">
                        <label>
                            Username</label>
                          <asp:TextBox ID="txtNamectm" runat="server" class="form-control input-lg"></asp:TextBox>
                        <%--<input type="text" value="" class="form-control input-lg">--%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">
                        <%--<a class="pull-right" href="#">(Lost Password?)</a>--%>
                        <label>
                            Password</label>
                        <asp:TextBox ID="txtPassword" runat="server" class="form-control input-lg" TextMode="Password"></asp:TextBox>
                        <%--<input type="password" value="" class="form-control input-lg">--%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <span class="remember-box checkbox">
                        <label for="rememberme">
                            <input type="checkbox" id="rememberme" name="rememberme">Remember Me
                        </label>
                    </span>
                </div>
                <div class="col-md-6">
                    <%--<input type="submit" value="Login" class="btn btn-primary pull-right push-bottom"
                        data-loading-text="Loading...">--%>
                    <asp:Button ID="BtnLogin" runat="server" Text="Login" class="btn btn-primary pull-right push-bottom" data-loading-text="Loading..."  />
                </div>
            </div>
            </form>
        </div>
       
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" runat="Server">
</asp:Content>

