﻿<%@ Page Title="CadcorpSIS Desktop GIS Map Editor, Map Manager, Map Modeller" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="product_cadcorp_DesktopGIS.aspx.vb" Inherits="product_cadcorp_DesktopGIS" %>

<%@ Register src="wuc_CadcorpMetaTag.ascx" tagname="wuc_HeadMetaTag" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<uc1:wuc_HeadMetaTag ID="wuc_HeadMetaTag1" runat="server" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

    <style type="text/css">
            .page-top
            {
                margin-bottom:0px;
                }
            .custom-product
            {               
                margin-bottom:35px;
                }
                
                      
    </style>

    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">									
						<li class="active">CadcorpSIS</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Desktop GIS</h1>
				</div>
			</div>
		</div>
	</section>

    <section class="page-top custom-product">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<h1>ซอฟท์แวร์ Desktop GIS จาก <strong> CadcorpSIS</strong>.</h1>
					<p class="lead">  เปิดไฟล์ข้อมูลแผนที่ได้ทุกนามสกุล!! ใช้งานสะดวกกว่าซอฟท์แวร์ทั่วไป
                                    ใช้ในการสร้าง แก้ไข และแปลงข้อมูลแผนที่ GIS ข้าม Format ได้กว่า 150 นามสกุล 
                                    เพื่อนำไปใช้ในแอพพลิเคชั่นต่างๆ ตามที่คุณต้องการ</p>
					<a href="contact.aspx" target="_blank" class="btn btn-default btn-lg push-bottom">ติดต่อเรา</a> <span class="arrow hlt" style="top: 10px;"></span>
                </div>
				<div class="col-sm-5">
					<img class="pull-right responsive"  src="img/cadcorp/DesktopGIS_Landing.png" "Powerful GIS Software">
				</div>
			</div>
		</div>
	</section>

    <div class="container">
        <div class="row">
		    <div class="col-sm-7">
			    <h2><strong>สุดยอดซอฟท์แวร์ GIS บน Desktop PC</strong></h2>
			    <p class="lead">
				    Desktop GIS Software ที่รวมรวมความสามารถในการจัดการข้อมูล GIS ตั้งแต่การเปิดไฟล์ การแก้ไขข้อมูล การจัดความเชื่อมโยงของโครงสร้างข้อมูลจากหลายสื่อ 
                    ไม่ว่าจะเป็นข้อมูลรูปแบบ File, Web Service, Database, OGC (Open GIS Consortium) และอื่นๆอีกมาก 
                    ที่สำคัญคือ CadcorpSIS Desktop GIS สามารถทำงานได้กับไฟล์ได้เกือบทุกนามสกุล(กว่า 150 Format) โดยเฉพาะความสามารถในการแปลงไฟล์ข้าม Format 
                    ที่เป็นจุดเด่นและเหนือกว่าซอฟท์แวร์ GIS ตัวอื่นๆที่มีอยู่ในตลาด ตลอดจนถึงความสามารถในการทำงานกับแผนที่ 3 มิติใน Map Modeller 
                    อีกทั้งลักษณะการใช้งานที่ง่ายกว่าซอฟท์แวร์ GIS ทุกตัว และราคา License ที่ต่ำกว่าคู่แข่ง
               </p>
		    </div>

		    <div class="col-sm-4 col-sm-offset-1 push-top">
			    <img class="img-responsive" src="img/cadcorp/desktop_200x200.png">
		    </div>
	    </div>

        <hr class="tall" />

        <div class="row custom-product"> 
		    <div class="col-sm-6">
			    <h2><strong>Map Manager</strong> </h2>			    
				    <div class="row">
			            <div class="col-md-12">				    				            
						 เพิ่มความสามารถจากตัว Map Express (Free) ให้สามารถสร้างและแก้ไขข้อมูลแผนที่ได้ทั้งหมด ไม่ว่าจะเป็น Line Point Polygon (Area)
                            รวมถึงความสามารถในการเชื่อมต่อกับฐานข้อมูลทั้ง Spatial Database(GIS) และ Relational Database(Attibutes)                    	
			            </div>
		            </div>
		    </div>
            <div class="col-sm-6 push-top">
                 <a href="http://www.cadcorp.com/products/desktop/map-manager" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-envelope"></i>เพิ่มเติม</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
            </div>
	    </div>

        <div class="row custom-product"> 
		    <div class="col-sm-6">               
			    <h2><strong>Map Editor</strong> </h2>			    
				    <div class="row">
			            <div class="col-md-12">				    				            
					    นอกเหนือจากความสามารถในการสร้างและแก้ไขข้อมูลแผนที่จาก Map Manager แล้ว Map Editor ยังเพิ่มความสามารถในการวิเคระาห์
                        เชิงสถิติที่ซับซ้อนรวมไปถึงการวิเคราะห์ Spatial Topology and Analysis ทางภูมิศาสตร์ และ Algorythm ขั้นสูง ซึ่งเป็นเครื่องมือวิเคราะห์ที่มีประสิทธิภาพ
                        สำหรับนักภูมิศาตร์                      	
			            </div>
		           </div>
		    </div>    
             <div class="col-sm-6 push-top">
                 <a href="http://www.cadcorp.com/products/desktop/map-editor" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-envelope"></i>เพิ่มเติม</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
            </div>
	    </div>

        <div class="row custom-product"> 
		    <div class="col-sm-6">               
			    <h2><strong>Map Modeller</strong> </h2>			    
				    <div class="row">
			            <div class="col-md-12">				    				            
						 	CadcorpSIS Map Modeller คือ GIS Software ที่ครอบคลุมการทำงานของงานในระบบบ GIS ทั้งหมดโดยความสามารถของ Map Modeller นั้น
                            ครอบคลุมการทำงานของทั้ง Map Manager และ Map Editor โดยได้เพิ่มความสามารถในการวิเคราะห์และ <b>การทำงานกับแผนที่ 3 มิติ!!</b> อีกทั้ง
                            ยังจำลองและสามารถสร้างพื้นผิว (Texture) และ Terrain ให้เหมือนสภาพภูมิประเทศจริง รวมไปถึงการสร้าง Thematic Map 
                            (การแสดงผลการวิเคราะห์เชิงพื้นที่ในมุมสนใจ ร่วมกับข้อมูลด้านต่างๆ ไม่ว่าจะเป็นสถิติด้านต่างๆ ข้อมูลเชิงกายภาพและคุณภาพ) เพื่อนำไปวิเคราะห์และการนำเสนอในโครงการตามที่ต้องการ		            
                         </div>
		           </div>
		    </div>    
             <div class="col-sm-6 push-top">
                 <a href="http://www.cadcorp.com/products/desktop/map-modeller" target="_blank" class="btn btn-primary btn-icon"><i class="fa fa-envelope"></i>เพิ่มเติม</a> <span class="arrow hlb" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
            </div>
	    </div>

        <hr class="tall" />

        <div class="row">
		    <div class="col-sm-6">
			    <h2><strong>ระบบปฏิบัติการ</strong> ที่รองรับ </h2>
			    
				    <div class="row">
			            <div class="col-md-12">				    				            
						        <div class="feature-box secundary">
							        <div class="feature-box-icon">
								        <i class="fa fa-windows"></i>
							        </div>
							        <div class="feature-box-info">
								        <h4 class="shorter">Microsoft Windows Vista</h4>								
							        </div>
						        </div>
						        <div class="feature-box secundary">
							        <div class="feature-box-icon">
								        <i class="fa fa-windows"></i>
							        </div>
							        <div class="feature-box-info">
								        <h4 class="shorter">Microsoft Windows 7</h4>								
							        </div>
						        </div>					           
						        <div class="feature-box secundary">
							        <div class="feature-box-icon">
								        <i class="fa fa-windows"></i>
							        </div>
							        <div class="feature-box-info">
								        <h4 class="shorter">Microsoft Windows 8</h4>								
							        </div>
						        </div>
                                <div class="feature-box secundary">
							        <div class="feature-box-icon">
								        <i class="fa fa-ban"></i>
							        </div>
							        <div class="feature-box-info">
								        <h4 class="shorter">ไม่รองรับ Microsoft Windows XP</h4>								
							        </div>
						        </div>			
			            </div>
		            </div>               
		    </div>
	    </div>
        
    </div>

     <section class="call-to-action featured footer">
		<div class="container">
			<div class="row">
				<div class="center">
					<h3>เรามีทีมตัวแทนจำหน่ายโดยตรงจาก CadcorpSIS ประเทศอังกฤษ คุณสามารถ<strong> <a href="contact.aspx" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">สอบถามข้อมูลเพิ่มเติม</a> </strong>
                    <br>เพื่อให้เรานำเสนอโดยไม่มีค่าใช้จ่าย
                    </h3>
				</div>
			</div>
		</div>
	</section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">
</asp:Content>

