﻿<%@ Page Title="Enterprise Resource Planning(ERP)" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="product_erp.aspx.vb" Inherits="product_erp" %>

<%@ Register src="~/wuc_ERPBIMetaTag.ascx" tagname="wuc_ERPBIMetaTag" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<uc1:wuc_ERPBIMetaTag ID="wuc_ERPBIMetaTag" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

    <section class="page-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">									
						<li class="active">Enterprise Monitoring Tools</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Enterprise Resource Planning(ERP)</h1>
				</div>
			</div>
		</div>
	</section>

    <div class="container">

					<h2><strong>ERP</strong> คืออะไร</h2>

					<div class="row">
						<div class="col-md-12">
							<p class="tall">
								Enterprise Resource Planning หรือ ERP แปลตรงๆสั้นๆ คือ "การวางแผนใช้ทรัพยากรขององค์กร
                                ให้เกิดประโยชน์ประโยชน์สูงสุด" ซึ่งในทางปฏิบัติ กระบวนการจัดการบริหารทรัพยากรนั้นจำเป็นจะต้องสอดคล้องกับ<a href="logistics.aspx" target="_blank">กิจกรรมทางโลจิสติกส์ขององค์กร</a>
                                ซึ่งแต่เดิม ERP แบ่งการจัดการเป็น 4 เรื่องใหญ่ๆหรือที่เรียกว่า 4M ประกอบไปด้วย Material, Machine ,Money และ Manpower ซึ่งจะเห็นได้ว่าการนำ ERP มาใช้ในองค์กรนั้น
                                สามารถครอบคลุมถึงกิจกรรมทางธุรกิจได้ทั้งองค์กร โดยการพัฒนาองค์กรโดยการนำหลัก ERP มาใช้มักทำควบคู่กับการทำ BPR (Business Process Re-Engineering) เพื่อให้ปรับปรุงการทำงาน
                                ให้มีประสิทธิภาพมากขึ้นอย่างต่อเนื่อง
							</p>
                            <p class="lead">
                                โดยปัจจุบันมีการนำซอฟท์แวร์ ERP มาใช้ในการบริหารจัดการองค์กรอย่างแพร่หลาย 
                                ซึ่งในมุมของซอฟท์แวร์ ERP นั้นแบ่งขอบเขตของ 4M เป็น 4 โมดูลใหญ่ๆดังนี้                                
                            </p>
						</div>						
					</div>

					<div class="row featured-boxes">
						<div class="col-md-3 col-sm-6">
							<div class="featured-box featured-box-primary">
								<div class="box-content">
									<i class="icon-featured fa fa-briefcase"></i>
									<h4>Marketing & Sales</h4>
									<p>
                                       ประกอบด้วย การดำเนินการตามคำสั่งซื้อ การตลาด การบริการและสนับสนุนลูกค้า การพยากรณ์พฤติกรรมความต้องการของลูกค้า การประชาสัมพันธ์โฆษณา
                                       การออกแบบบรรจุภัณฑ์
                                        <%--<a href="/" class="learn-more">learn more <i class="fa fa-angle-right"></i></a>--%>
                                    </p>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="featured-box featured-box-secundary">
								<div class="box-content">
									<i class="icon-featured fa fa-cog"></i>
									<h4>Manufacturing & Production Management</h4>
									<p>
                                    การบริหารวัตถุดิบ การบริหารซัพพลายเออร์ การจัดซื้อ การรับวัตถุดิบ การขนส่งวัตถุดิบ 
                                    การขนส่งสินค้า การบริหารการผลิต 
                                    การบริหารคลังสินค้าและวัตถุดิบ การตรวจสอบคุณภาพ การจัดการสินค้าที่ถูกส่งกลับคืน
                                    การบำรุงรักษาปัจจัยการผลิตและการจัดเตรียมอะไหล่สำรอง
                                    <%--<a href="/" class="learn-more">learn more <i class="fa fa-angle-right"></i></a>--%>
                                    </p>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="featured-box featured-box-tertiary">
								<div class="box-content">
									<i class="icon-featured fa fa-dollar"></i>
									<h4>Finalcial & Accounting</h4>
									<p>การบริหารบัญชีการเงิน การควบคุมต้นทุน การวางแผนและจัดทำงบประมาณ การบริหารกระแสเงินสด
                                        การพยากรณ์งบดุลย์ การบริหารภาษี
                                        <%--<a href="/" class="learn-more">learn more <i class="fa fa-angle-right"></i></a>--%>
                                    </p>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="featured-box featured-box-quartenary">
								<div class="box-content">
									<i class="icon-featured fa fa-user"></i>
									<h4>Human Resource Management</h4>
									<p>การบริหารอัตรากำลัง การจ่ายผลตอบแทน การควบคุมวินัยในการทำงาน การพัฒนาทรัพยากรบุคคล
                                        การติดต่อสื่อสารภายกับผู้ที่เกี่ยวข้องทั้งภายในและภายนอกองค์กร
                                        <%--<a href="/" class="learn-more">learn more <i class="fa fa-angle-right"></i></a>--%>
                                    </p>
								</div>
							</div>
						</div>
					</div>
                    <hr>
					<div class="row push-top">
						<div class="col-md-12">
							<h2>การให้บริการของเรา <strong>(หากคุณเกี่ยวข้องอยู่แล้วหรือกำลังจะเกี่ยวข้องกับซอฟท์แวร์ ERP)</strong></h2>
							<div class="row">
								<div class="col-sm-6">
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-comment"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">บริการให้คำปรึกษาในการวางแผนเลือกซอฟท์แวร์ ERP สำหรับองค์กร (ไม่มีค่าใช้จ่าย)</h4>
											<p class="tall">
                                                ด้วยความสามารถของซอฟท์แวร์ ERP คุณคงเห็นแล้วว่าการนำซอฟท์แวร์ ERP มาใช้ในองค์กรนั้น 
                                                จะครอบคลุมการจัดการทุกกิจกรรมในองค์กร ดังนั้นซอฟท์แวร์ ERP ส่วนใหญ่จึงมีราคาสูงตั้งแต่ <strong>หลักแสนบาทไปจนถึงหลายร้อยล้าน</strong>
                                                ดังนั้นการเลือกหาซอฟท์แวร์ ERP มาใช้เป็นเรื่องที่สำคัญที่ต้องวางแผนอย่างรอบคอบ จากประสบการณ์ของเราพบว่าซอฟท์แวร์ ERP ทุกยี่ห้อ
                                                มีจุดแข็งและจุดอ่อนต่างกันไป จากประสบการณ์ของเราพบว่ามีไม่น้อยถึง <strong>70% ตัดสินใจซื้อ ERP มาใช้แต่ไม่ประสบความสำเร็จ</strong>
                                                และส่วนใหญ่หลังจากลงทุนไปด้วยเงินมหาศาลแล้ว<strong>ไม่สามารถใช้งานกับองค์กรของตนได้</strong> เราจึงยินดีให้คำปรึกษาสำหรับท่านในการเลือกซื้อซอฟท์แวร์ ERP 
                                                <strong>โดยไม่คิดค่าใช้จ่ายใดๆทั้งสิ้น(เพราะเราเองไม่ได้เป็นตัวแทนจำหน่ายซอฟท์แวร์ ERP)</strong>
                                                โดยทีมงานหวังเพียงแค่ให้คนไทยประสบความสำเร็จในการนำซอฟท์แวร์และเทคโนโลยีมาใช้ปรับปรุงประสิทธิภาพการทำงานในองค์กรก็ถือว่าบรรลุเป้าหมายของเราแล้ว
                                             </p>
										</div>
									</div>
                                    <div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-desktop"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">บริการพัฒนาซอฟท์แวร์ ERP เฉพาะสำหรับองค์กรธุรกิจของท่าน</h4>
											<p class="tall">
                                                จะเห็นได้ว่าซอฟท์แวร์ ERP ครอบคลุมการจัดการทุกกิจกรรมในองค์กร ดังนั้นส่วนใหญ่จึงมีราคาสูงตั้งแต่ <strong>หลักแสนบาทไปจนถึงหลายร้อยล้าน</strong>
                                                ที่ผ่านมาเราพบว่าการซื้อซอฟท์แวร์ ERP สำเร็จรูปนั้นไม่สามารถใช้งานได้ทันทีเพราะธุรกิจของไทยถึงแม้เป็นธุรกิจประเภทเดียวกันแต่<strong>มีความจำเพาะเจาะจงสูง</strong>
                                                ต่างกับระบบธุรกิจต่างประเทศซึ่งมักมีวิธีการทำงานเหมือนๆกัน ดังนั้นหลังจากซื้อซอฟท์แวร์ราคาสูงแล้ว ยังต้อง Customize ซอฟท์แวร์สำเร็จรูปเหล่านั้น
                                                ให้รองรับกับการทำงานสำหรับเฉพาะองค์กรของท่าน บ้างก็หลายเดือน ยิ่งองค์กรใหญ่ <strong>Customize หลายปีกว่าจะได้ใช้</strong> บ้างก็เลิกกลางคันสูญเงินมหาศาล
                                                หลายท่านคงมีประสบการณ์เรื่องนี้ ดังนั้นทีมงานจึงเสนอแนวทาง<strong>การพัฒนาซอฟท์แวร์ ERP เฉพาะสำหรับองค์กรของท่านแบบค่อยเป็นค่อยไป</strong> โดยเน้นไปยังระบบงานหลักที่สำคัญ
                                                แล้วขยายผลไปยังระบบงานอื่นๆตามมา <strong><a href="logistics.aspx">เรามีทีมที่ปรึกษา Logistic ช่วยในการวิเคราะห์ความต้องการของระบบ</a></strong>
                                                ซึ่งเป็นจุดแข็ง ทำให้คุณมั่นใจว่างาน ERP ที่ออกมาจะสามารถใช้งานได้ตรงตามเป้าหมาย ช่วยลดความเสี่ยงในการลงทุน และระบบงานใดพัฒนาเสร็จก็สามารถใช้งานได้ทันทีไม่ต้องรอให้ Customize เสร็จทั้งองค์กร
                                                อีกทั้งยังลดค่าใช้จ่าย เพราะค่าใช้จ่ายหลักๆของซอฟท์แวร์ ERP ที่ขายในเมืองไทยคือ<strong>ค่า Customize โดยมักเทียบเท่าหรือสูงกว่าราคาซอฟท์แวร์หลายเท่า</strong>
                                                โดยที่ผ่านมา<strong>เราได้พัฒนาระบบซอฟท์แวร์ ERP ให้กับลูกค้ามาแล้วหลายประเภทธุรกิจ</strong> โดยเฉพาะบริษัทยักษ์ใหญ่วงการพลังงานของไทยและกลุ่มในเครือ 
                                                โดยปัจจุบันลูกค้าเหล่านี้ยังคงพัฒนาต่อยอด ขยายผลจากระบบเดิมๆอย่างต่อเนื่อง ตามแนวทางที่ทุกฝ่ายวางไว้
                                            </p>
										</div>
									</div>
									
								</div>
								<div class="col-sm-6">
                                    <div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-wrench"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">บริการปรับปรุงแก้ไขเพิ่มเติมซอฟท์แวร์ ERP เดิมที่มีปัญหา</h4>
											<p class="tall">
                                            ด้วยการที่ซอฟท์แวร์ ERP ครอบคลุมการจัดการทุกกิจกรรมในองค์กรจึงยากที่จะเปลี่ยนแปลงระบบ 
                                            ดังนั้นหากการวางแผนในการติดตั้งระบบในครั้งแรกไม่ดีพอ จึงมักเกิดปัญหาเมื่อใช้งานไปนานๆ ไม่ว่าจะเป็นการเข้ากันได้กับอุปกรณ์สำนักงานใหม่ๆ
                                            ความล่าช้าในการใช้งาน การที่ไม่สามารถเชื่อมโยงกับระบบอื่นๆได้ทำให้เจ้าหน้าที่ต้องทำงานซ้ำซ้อน ปัญหาเสถียรภาพระบบ
                                            อีกทั้งยังไม่สามารถติดต่อผู้ให้บริการเจ้าเดิมได้ หากคุณเกิดปัญหาเหล่านี้ ลองโทรปรึกษาเรา เพราะเรามีทีมผู้เชี่ยวชาญในการแก้ปัญหา
                                            ซอฟท์แวร์ ERP รวมถึงระบบซอฟท์แวร์ทั่วไป เพราะเรามีแนวทางการแก้ปัญหาที่หลากหลาย ขึ้นอยู่กับหลายปัจจัย และเรามักหาทางออกที่เหมาะสมให้คุณได้เสมอ
                                            </p>
										</div>
									</div>
									<div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-line-chart"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">บริการพัฒนาต่อยอดความสามารถซอฟท์แวร์ ERP เดิม</h4>
											<p class="tall">เพราะเราคือบริษัท ไทยอิเมจิเนชั่นเทคโนโลยี จำกัด เราจึงมีทีมวิศวกรระบบ คอยวิเคราะห์แนวทางการพัฒนาต่อยอดงานระบบ ERP เดิม
                                            รวมถึงระบบงานซอฟท์แวร์ทั่วไปให้สามารถขยายผลรองรับการทำงานใหม่ๆได้ตามที่คุณต้องการ ไม่ว่าจะเป็นการเพิ่มรายงาน Print Out <a href="product_bi.aspx">การทำ Dashboard
                                            เพื่อสรุปภาพรวม</a>ช่วยในการิเคราะห์ด้านต่างๆ การปรับ Flow การทำงานเดิม รวมถึงวิเคราะห์แนวทางการวางสถาปัตยกรรมระบบให้รองรับสถาปัตยกรรมแนวใหม่
                                            ตามความต้องการของคุณ เพื่อ<strong>ให้ระบบที่ใช้งานอยู่ยังสามารถใช้งานได้ต่อไปโดยไม่ต้องซื้อระบบใหม่</strong>ในขณะเดียวกันก็เพิ่มความสามารถใหม่ๆลงไปได้ตามต้องการ                   
                                            </p>
										</div>
									</div>
                                    <div class="feature-box">
										<div class="feature-box-icon">
											<i class="fa fa-mortar-board"></i>
										</div>
										<div class="feature-box-info">
											<h4 class="shorter">บริการที่ปรึกษาการปรับปรุงการจัดการโลจิสติกส์สำหรับองค์กร</h4>
											<p class="tall">ด้วยวิทยากรและทีมงานที่ปรึกษา ระดับแนวหน้าของประเทศ ซึ่งมีประสบการณ์ในการให้คำปรึกษาในการพัฒนาโลจิสติกส์ในองค์กรมาแล้วกว่า 11 ปี
                                            เราสามารถวิเคราะห์และ<strong><a href="logistics.aspx">ให้คำแนะนำในการปรับปรุงการจัดการโลจิสติกส์</a></strong>ในองค์กรของคุณ อีกทั้งยังมีความยินดีให้บริการบรรยายเพื่อเสริมสร้าง
                                            ความรู้ความเข้าใจและแนวคิดที่ดี ในการทำงานให้กับพนักงานของในองค์กรคุณ</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>

					<hr class="tall" />

					<div class="row">
						<div class="col-sm-6">
							<h2>ประสบการณ์ของเราเกี่ยวกับ <strong>ซอฟท์แวร์ ERP</strong></h2>
							<p class="lead">
								เราเคยผ่านงานพัฒนาและ Customize ซอฟท์แวร์ ERP ให้กับองค์กรหลากหลายประเภทธุรกิจ รวมถึงองค์กรธุรกิจพลังงานยักษ์ใหญ่ของประเทศและกลุ่มบริษัทในเครือ
							</p>
							<p class="tall">
								จากประสบการณ์ดังกล่าวร่วมกับนโยบายของบริษัท ที่นักพัฒนาระบบทางเทคนิคต้องทำงานควบคู่ร่วมไปกับที่ปรึกษาโลจิสติก นี่คือกุญแจสำคัญที่ทำให้เราประสบความสำเร็จ
                                ในโครงการพัฒนาระบบซอฟท์แวร์ ERP สำหรับองค์กร โดย<strong>งานที่เราส่งมอบปัจจุบันสำเร็จ 100%</strong> และช่วยแก้ปัญหาในการทำงานให้กับลูกค้าได้เป็นอย่างมาก
                                ด้วยแนวคิดที่ไม่ได้ทำแค่หวังผลกำไร แต่ด้วยแนวคิดที่เรายึดถือมาตั้งแต่เริ่มคือ 

                                "เพื่อพัฒนาความยั่งยืนในแนวความคิด" ให้แก่ทั้งลูกค้า พันธมิตรผู้ร่วมงาน
                                และภายในทีมงานของเราเอง โดยการปลูกฝังแนวความคิดเชิงนวัตกรรมนั้น และเราถือว่าเป็นผลกำไรจับต้องไม่ได้แต่มีมูลค่ามหาศาลที่ทั้งทีมงานและลูกค้าจะได้รับ 
                                ซึ่งเราเชื่อว่า นี่คือสิ่งสำคัญที่ทำให้เราพัฒนาร่วมกันต่อไปอย่างยั่งยืน
							</p>
						</div>
						<div class="col-sm-6 push-top">
							<img class="img-responsive" src="img/products/ERP-Software.jpg" alt="โมดูลของซอฟท์แวร์ ERP" />
						</div>
					</div>
				</div>

     <section class="call-to-action featured footer">
		<div class="container">
			<div class="row">
				<div class="center">
					<h3>หากต้องการข้อมูลเพิ่มเติม <a href="contact.aspx" target="_blank" class="btn btn-lg btn-primary" data-appear-animation="bounceIn">ติดต่อเรา</a> <span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span>
                    เรายินดีให้บริการ
                    </h3>
				</div>
			</div>
		</div>
	</section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">
</asp:Content>

