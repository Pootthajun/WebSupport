﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="contact.aspx.vb" Inherits="contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<meta name="keywords" content="" id="metaKeyword" runat="server" />
<meta name="description" content="ติดต่อเรา บริษัท ไทย อิเมจิเนชั่นเทคโนโลยี จำกัด บริษัทที่ปรึกษาพัฒนาซอฟท์แวร์ วิเคราะห์ระบบธุรกิจเพื่อออกแบบระบบฐานข้อมูล พัฒนาระบบเทคโนโลยีสานสนเทศและสารสนเทศภูมิศาสตร์ GIS เพื่อใช้ในธุรกิจ บริการที่ปรึกษาพัฒนาระบบการจัดการโลจิสติกส์สำหรับองค์กร บริการแยกส่วนเครื่องบินเหมาลำเพื่อส่งออกชิ้นส่วนอลูมิเนียมและไททาเนียมเพื่อนำไปผลิตเป็นวัตถุดิบในอุตสาหกรรมใหม่" />
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceholder" Runat="Server">

    <section class="page-top">
		<div class="container">						
			<div class="row">
				<div class="col-md-12">
					<h1>ข้อมูลติดต่อ</h1>
				</div>
			</div>
		</div>
	</section>

	<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
	<div id="googlemaps" class="google-map"></div>

	<div class="container">
        <asp:UpdatePanel ID="udpEmail" runat="server">
        <ContentTemplate>
		    <div class="row">
			    <div class="col-md-6">
				    

				    <h2 class="short"><strong>Contact</strong> (ติดต่อเรา)  </h2>
                        <div class="row">
						    <div class="form-group">
							    <div class="col-md-6">
								    <label>(ชื่อ) Name *</label>
								    <asp:TextBox ID="txtName" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                </div>
							    <div class="col-md-6">
								    <label>(อีเมลของคุณ) Email Address *</label>
                                    <asp:TextBox ID="txtFrom" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>								
							    </div>
						    </div>
					    </div>
                        <div class="row">
						    <div class="form-group">
							    <div class="col-md-12">
								    <label>(เบอร์ติดต่อ) Phone (option)</label>
								    <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control"  maxlength="100"></asp:TextBox>
                                </div>
						    </div>
					    </div> 
					    <div class="row">
						    <div class="form-group">
							    <div class="col-md-12">
								    <label>(เรื่อง) Subject *</label>
								    <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control"  maxlength="100"></asp:TextBox>
                                </div>
						    </div>
					    </div>                        
					    <div class="row">
						    <div class="form-group">
							    <div class="col-md-12">
								    <label>(ข้อความถึงเรา) Message *</label>
                                    <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" maxlength="5000" Rows="10" CssClass="form-control"></asp:TextBox>
							    </div>
						    </div>
					    </div>
					    <div class="row">
						    <div class="col-md-12">
                                <asp:Button Text="(ส่งข้อความ) Send Message" ID="btnSend" runat="server" CssClass="btn btn-primary btn-lg" />						
						    </div>
					    </div>
                        <div class="row" style="margin-bottom:20px;">
                        
                        </div>
                     <div class="alert alert-success hidden" id="contactSuccess" runat="server">
					    <strong>Success!</strong> ขอบคุณครับเราได้รับข้อความแล้ว (Your message has been sent to us)
				    </div>

				    <div class="alert alert-danger hidden" id="contactError" runat="server">
					    <strong>Something wrong!</strong> <asp:Label ID="lblError" runat="server"></asp:Label>
				    </div>
    		    </div>
			    <div class="col-md-6">
				    <h4 class="push-top">Get in <strong>touch</strong></h4>
				    <p>เรายินดีให้คำปรึกษา และมีส่วนร่วมในความสำเร็จของคุณ
                    โดยสามารถติดต่อเราได้ตามช่องทางด้านล่างนี้</p>
				    <hr />
				    <h4>ที่ตั้ง <strong>สำนักงาน</strong> (Office)</h4>
				    <ul class="list-unstyled">
					    <li><i class="fa fa-map-marker"></i> <strong>ที่อยู่ : </strong>บริษัท ไทย อิเมจิเนชั่น เทคโนโลยี จำกัด (สำนักงานใหญ่) 74/10  ซอยลาดพร้าว93 (โชคชัย3) ถ.ลาดพร้าว แขวงวังทองหลาง เขตวังทองหลาง กรุงเทพฯ 10310</li>
                        <li><i class="fa fa-map-marker"></i> <strong>Address : </strong>Thai Imagination Technology Co.,Ltd. 74/10 Soi Ladprap93 Ladprap Rd. Kwaeng Wangtonglaang Khet Kwaeng Wangtonglaang Bangkok 10310</li>
					    <%--<li><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-7890</li>--%>
					    <li><i class="fa fa-envelope"></i> <strong>Email : </strong> <a href="mailto:info@tit-tech.co.th">info@tit-tech.co.th</a></li>
                        <li><i class="fa fa-envelope"></i> <strong>Email : </strong> <a href="mailto:support@tit-tech.co.th">support@tit-tech.co.th</a></li>
				    </ul>
				    <hr />
				    <h4>ช่วงเวลา <strong>ทำการ</strong> (Hours)</h4>
				    <ul class="list-unstyled">
					    <li><i class="fa fa-time"></i> MON - FRI 9am to 6pm</li>
					    <li><i class="fa fa-time"></i> SAT - 11am to 4pm</li>
					    <li><i class="fa fa-time"></i> SUN - Closed</li>
				    </ul>
			    </div>
		    </div>
        </ContentTemplate>
        </asp:UpdatePanel>
	</div>


    <section class="call-to-action featured footer">
		<div class="container">
			<div class="row">
				<div class="center">
					<h3>ทีมงาน <strong>บริษัท ไทยอิเมจิเนชั่น เทคโนโลยี จำกัด</strong> ขอขอบคุณทุกข้อความที่ส่งถึง 
                    <br>และขอขอบคุณทุกท่านที่เปิดโอกาสให้เรา <strong>มีส่วนร่วมในความสำเร็จ</strong> ของคุณ
                    </h3>
				</div>
			</div>
		</div>
	</section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceholder" Runat="Server">


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCk0tPeDdULOe_2fyIt5R5c17PIyTG4hLU"></script>
<script type="text/javascript">

		    /* Map Markers
		    var mapMarkers = [{
		        address: "Thai Imagination Technology",
		        html: "<strong>Thai Imagination Technology</strong><br>Ladprao, Bangkok 10310",
		        icon: {
		            image: "img/TIT.png",
		            iconsize: [26, 46],
		            iconanchor: [12, 46]
		        },
		        popup: true
		    }];*/

		    /* Map Initial Location

		    

		    // Map Extended Settings
		    var mapSettings = {
		        controls: {
		            draggable: true,
		            panControl: true,
		            zoomControl: true,
		            mapTypeControl: true,
		            scaleControl: true,
		            streetViewControl: true,
		            overviewMapControl: true
		        },
		        scrollwheel: false,
		        markers: mapMarkers,
		        latitude: initLatitude,
		        longitude: initLongitude,
		        zoom: 16
		    };

		    var map = $("#googlemaps").gMap(mapSettings);

		    // Map Center At
		    var mapCenterAt = function (options, e) {
		        e.preventDefault();
		        $("#googlemaps").gMap("centerAt", options);
		    }*/


    var map;
    var initLatitude = 13.782019;
    var initLongitude = 100.6216;
    function initMap() {

        var mapTypeIds = [];
        for (var type in google.maps.MapTypeId) {
            mapTypeIds.push(google.maps.MapTypeId[type]);
        }
        mapTypeIds.push("OSM");

        var mapOptions = {
            center: new google.maps.LatLng(initLatitude, initLongitude),
            zoom: 16,
            scrollwheel: false,
            streetViewControl: true,
            scaleControl: false,
            panControl: false,
            //mapTypeId: "OSM",
            mapTypeControlOptions: {
                mapTypeIds: mapTypeIds,
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_LEFT
            },
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.LEFT_TOP
            }
        };

        map = new google.maps.Map(document.getElementById("googlemaps"), mapOptions);
        map.mapTypes.set("OSM", new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
            },
            tileSize: new google.maps.Size(256, 256),
            name: "OpenStreetMap",
            maxZoom: 18
        }));

        //Add Marker
        var infowindow = new google.maps.InfoWindow({
            content: "<h4>Thai Imagination Technology</h4>Ladprap93 Wangtonglaang Bangkok Thailand 10310"
        });

        var marker = new google.maps.Marker({
            title: "Thai Imagination Technology",
            position: new google.maps.LatLng(initLatitude, initLongitude),
            map: map,           
            icon: 'img/TITPIN.png'
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });

    }
    google.maps.event.addDomListener(window, 'load', initMap);
   
</script>


</asp:Content>

